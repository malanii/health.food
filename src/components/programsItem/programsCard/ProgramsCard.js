import React, {Component} from 'react';
import s from './programsCard.module.scss';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleRight} from "@fortawesome/free-solid-svg-icons";
import {connect} from "react-redux";
import {addToCartPrograms} from "../../../store/actions/cartAction";


export class ProgramsCard extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        // this.state = {
        //     visibility: false
        // }
    }


    handleClick = (e) => {

        let currentProgram = this.props;
        const {itemsProgram, itemsDietologs} = this.props.cart;
        if (itemsProgram.length === 0) {
            this.props.addToCartPrograms(e,currentProgram);
        } else {
            let existed_item = itemsProgram.find(item => item.itemNo === currentProgram.itemNo);
            if (existed_item) {
                console.log('exist')
                // this.setState(() => {
                //     return {
                //         visibility: true,
                //     };
                // });
                // setTimeout(() => this.setState(() => {
                //     return {
                //         visibility: false,
                //     };
                // }), 2000);
            } else {

                this.props.addToCartPrograms(e,currentProgram)
            }
        }
    };


    render() {
        // const existItemMessage = this.state.visibility ? 'exist-item-show' : 'exist-item-hidden';
        return (
            <div key={this.props._id} className={s.card}>
                <div className={s.header}>
                    <img src={this.props.imageUrls} className={`col-12 ${s.img}`}/>
                    <p className={s.daysWrapper}>{this.props.quantity} дней</p>
                </div>

                <div className={s.contentWrapper}>
                    <p className={s.title}>{this.props.name}</p>
                    <div className='flex-center'>
                        <p className={s.title}>{this.props.currentPrice}</p>
                        <img className={s.priceImg} src='/img/details/uah.png' alt='uah'/>
                    </div>
                </div>

                <button onClick={(e) => {
                    this.handleClick(e)
                }} className={`link-button link-button-small ${s.buttonBuy}`}>Купить
                </button>
                <p className={s.additionalText}>просмотреть 3 дня</p>
                <div className={s.button}>
                    <Link className='' to={`/programs/${this.props.itemNo}`}><FontAwesomeIcon
                        className={s.icon} icon={faAngleDoubleRight}/></Link>
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    };
};
export default connect(mapStateToProps, {addToCartPrograms})(ProgramsCard);

