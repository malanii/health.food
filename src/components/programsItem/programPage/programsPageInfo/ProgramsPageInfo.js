import React, {Component} from 'react';
import s from './programsPageInfo.module.scss';
import {addToCartPrograms} from "../../../../store/actions/cartAction";
import {connect} from "react-redux";



export class ProgramsPageInfo extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = (e) => {
        let currentProgram = this.props;
        const {itemsProgram, itemsDietologs} = this.props.cart;
        if (itemsProgram.length === 0) {
            this.props.addToCartPrograms(e,currentProgram);
        } else {
            let existed_item = itemsProgram.find(item => item.itemNo === currentProgram.itemNo);
            if (existed_item) {
                console.log('est uzhe v korzine')
            } else {
                this.props.addToCartPrograms(e,currentProgram)
            }
        }
    };
    render() {
        return (
            <div className={s.wrapper}>
                <div key={this.props._id} className={`${s.header} flex-column flex-sm-row`}>
                    <p className={s.name}>{this.props.name}</p>
                    <button  className={s.button} onClick={() => {
                        this.handleClick(this.props.itemNo)
                    }} >Купить
                    </button>
                </div>
                    <div className='flex-around  flex-center flex-column flex-md-row' >
                        {this.props.description.map(el =>
                            <div className='col-9 col-sm-5 col-md-3 py-3 ml-5 mr-5'>
                                <p className={s.textNumber}>{el.number}</p>
                                <p className={s.text}>{el.text}</p>
                            </div>
                        )}
                    </div>

                <div className={`${s.conclusionWrapper} flex-column flex-lg-row`}>
                    {this.props.conclusions.map(el =>
                        <div  className='col-12 col-lg-3'>
                            <img className={s.conclusionImg} src={el.icon} alt="home"/>
                            <p className={s.conclusionTitle}>{el.param}</p>
                            <p className={s.conclusionText}>{el.text}</p>
                        </div>
                    )}
                </div>

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    };
};
export default connect(mapStateToProps,{addToCartPrograms})(ProgramsPageInfo);