import React, {Component} from 'react';
import s from './programsPageNutrition.module.scss'

export default class ProgramsPageNutrition extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const nutrition = this.props.weeklyCount;
        return (
            <div className={s.wrapper}>
                {nutrition.map(result =>
                    <div className={`col-7 col-md-5 col-lg-3 ${s.card}`}>
                        <img src={result.imageUrls} className={s.img} alt='photo1'/>
                        <p className={s.title}>{result.dayName}</p>

                        {result.schedule.map(day =>
                            <div className={s.scheduleWrapper}>
                                <p className={`col-3 ${s.text}`}>{day.time}</p>
                                <div className='col-9 text-left'>
                                    <p className={s.text}>{day.food}</p>
                                    <p className={s.text}>Порция: <span>{day.portion}</span></p>
                                </div>
                            </div>
                        )}
                    </div>)}
            </div>
        )
    }
}

