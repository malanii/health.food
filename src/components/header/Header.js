 import React, {Component} from 'react';
import s from './header.module.scss';
import Title from "../common/text/title/Title";
import Description from "../common/text/description/Description";
import LinkButton from "../common/button/linkButton/LinkButton";

export default class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            button: {
                link: '/programs',
                name: 'Начать сейчас',
                size: 'big'
            },
            title: 'Правильное питание - это не временная диета на месяц.',
            description: ["Это перестройка системы питания и изменение пищевых привычек.",
                "Начинайте исправлять пищевое поведение уже с завтрешнего дня ."]
        }
    }

    render() {
        return (
            <div className={s.container}>
                <div className={` ${s.text}`}>
                    <Title name={this.state.title}/>
                    {this.state.description.map((p) =>
                        <Description name={p}/>
                    )}

                    <div className={`${s.link} `}>
                        <LinkButton {...this.state.button}/>
                    </div>
                </div>
                <img className={s.img} src='./img/details/header1_.png' alt="header"/>
            </div>
        )
    }
}