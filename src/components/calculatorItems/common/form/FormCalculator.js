import React from "react";
import s from "./formCalculator.module.scss";



export default class FormCalculator extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <form onSubmit={this.props.submitHandler} className='col-8 m-auto'>
                <div className={s.container}>
                    {this.props.parameters.map(e =>
                        <div className=' text-center ml-2 mt-2'>
                            <p className={s.text}>{e.field}</p>
                            <input className={s.input} type='text'
                                   data-name={e.name}
                                   name={e.name}
                                   value={e.val}
                                   onChange={this.props.changeHandler}/>
                        </div>
                    )}
                </div>
                <div className={s.buttonWrapper}>
                    <button type='submit' className='link-button link-button-big m-auto'>Расчитать</button>
                </div>
                <p className={s.tinyText}>* Полученные данные не могут трактоваться как профессиональные медицинские рекомендации и предоставляются исключительно в ознакомительных целях.</p>
            </form>
        )
    }
}
