import React from "react";
import Dropdown from "react-bootstrap/Dropdown";
import s from './genderParametr.module.scss'
export default class GenderParametr extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <Dropdown className={`col-4 ${s.dropdown}`}>
                <Dropdown.Toggle className='drop-test' variant="success"
                                 id="dropdown-basic">{this.props.currentGender}
                </Dropdown.Toggle>
                <Dropdown.Menu className='drop-test '>
                    {this.props.gender.map(el =>
                        <Dropdown.Item onClick={this.props.handleGender} data-name={el.data}>
                            {el.name}
                        </Dropdown.Item>)
                    }
                </Dropdown.Menu>
            </Dropdown>
        )
    }
}
// gender: [{name: 'мужчина', data: 'men'}, {name: "женщина", data: 'women'}],
// handleGender = (e) => {
//     this.setState({
//         ...this.state,
//         currentGender: e.target.innerHTML
//     })
// };