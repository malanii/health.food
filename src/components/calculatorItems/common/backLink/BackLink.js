import React from "react";
import {Link} from 'react-router-dom';
import s from './backLink.module.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleLeft} from "@fortawesome/free-solid-svg-icons";

export default class BackLink extends React.Component {

    render() {
        return (
            <Link to='/calculator' className={s.button}>
                <FontAwesomeIcon className={s.buttonIcon} icon={faAngleDoubleLeft}/>
            </Link>
        )
    }
}
