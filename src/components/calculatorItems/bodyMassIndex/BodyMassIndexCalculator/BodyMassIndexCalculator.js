import React from "react";
import s from './bodyMassIndexCalculator.module.scss';
import FormCalculator from "../../common/form/FormCalculator";
import GenderParametr from "../../common/gender/GenderParametr";
import BodyMassIndexResults from "../BodyMassIndexResults/BodyMassIndexResults";
import BackLink from "../../common/backLink/BackLink";

export default class BodyMassIndexCalculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parameters: [{field: 'Рост, см', name: "height", val: ""}, {field: 'Вес, кг', name: "weight", val: ""},],
            gender: [{name: 'Мужчина', data: 'men'}, {name: "Женщина", data: 'women'}],
            currentGender: 'Стать',
            result: 0,
        };
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
        this.handleGender = this.handleGender.bind(this);
    }

    changeHandler = (e) => {
        let newItems = this.state.parameters.map(item => item.name === e.target.dataset.name
            ? {...item, val: e.target.value}
            : item);
        this.setState({
                parameters: newItems
            }
        );
        e.target.value.length >= 1 && e.target.classList.remove('input-mistake');
    };

    validate = (e) => {
        const [height, weight] = this.state.parameters;
        let elems = document.querySelectorAll('input');
        if (!height.val || isNaN(height.val)) {
            elems[0].classList.add('input-mistake')
        }
        if (!weight.val || isNaN(weight.val)) {
            elems[1].classList.add('input-mistake')
        }

        if ((!isNaN(height.val) && height.val) && (!isNaN(weight.val) && weight.val) && this.state.currentGender !== "Стать") {
            return true
        }
    };

    submitHandler = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const [height, weight] = this.state.parameters;
            let m = parseInt(weight.val);
            let h = parseInt(height.val) / 100;
            let index = Math.round(m / (h * h));
            this.setState({
                ...this.state,
                result: index
            });
        }
    };
    handleGender = (e) => {
        this.setState({
            ...this.state,
            currentGender: e.target.innerHTML
        })
    };

    render() {
        return (
            <div className={`col-8 ${s.wrapper}`}>

                {
                    this.state.result === 0
                        ? [<div>
                            <BackLink/>
                            <p className={s.title}>Индекс массы тела</p>
                            <p className={s.text}>это показатель, оценивающий соответствие роста и массы человека и
                                позволяющий определить находится ли его масса тела в пределах нормы</p>
                            <GenderParametr currentGender={this.state.currentGender} gender={this.state.gender}
                                            handleGender={this.handleGender.bind(this)}

                            />
                            <FormCalculator parameters={this.state.parameters}
                                            submitHandler={this.submitHandler.bind(this)}
                                            changeHandler={this.changeHandler.bind(this)}

                            />
                        </div>]
                        : <BodyMassIndexResults result={this.state.result}/>
                }

            </div>
        )
    }
}
