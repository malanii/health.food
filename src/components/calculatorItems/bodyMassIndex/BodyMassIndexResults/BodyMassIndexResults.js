import React from "react";
import s from './bodyMassIndexrResults.module.scss'
import BackLink from "../../common/backLink/BackLink";

export default class BodyMassIndexResults extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div className={s.wrapper}>
                <BackLink/>
                <p className={s.title}>Индекс массы тела: <span className={s.additionalText}>{this.props.result}</span></p>
                <p className={s.text}>Интерпретация показателей проводится соответственно рекомендациям всемирной организации здравоохранения. Показатель ИМТ при нормальной массе тела находится в пределах от 18.5 до 25, если ниже, то масса недостаточная, выше – избыточная.</p>
            </div>
        )
    }
}
