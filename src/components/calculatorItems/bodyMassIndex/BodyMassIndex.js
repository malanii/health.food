import React from "react";
import {Link} from 'react-router-dom';
import s from './bodyMassIndex.module.scss'

export default class BodyMassIndex extends React.Component {

    render() {
        return (
            <Link to='body_mass_index' className={s.wrapper}>
                <p className={s.text}>Индекс массы тела</p>
            </Link>
        )
    }
}
