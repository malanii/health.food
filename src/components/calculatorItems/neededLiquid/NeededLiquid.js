import React from "react";
import {Link} from 'react-router-dom';
import s from './neededLiquid.module.scss';

export default class WasteOfCalories extends React.Component {

    render() {
        return (
            <Link to='/needed_liquid' className={s.wrapper}>
                <p className={s.text}>Расчет физиологической потребности в жидкости</p>
            </Link>
        )
    }
}