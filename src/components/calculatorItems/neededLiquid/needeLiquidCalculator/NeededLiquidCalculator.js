import React from "react";

import s from './neededLiquidCalculator.module.scss'

import FormCalculator from "../../common/form/FormCalculator";
import GenderParametr from "../../common/gender/GenderParametr";
import NeededLiquidResults from "../neededLiquidResults/NeededLiquidResults";
import BackLink from "../../common/backLink/BackLink";


export default class NeededLiquidCalculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parameters: [{field: 'Вес, кг', name: "weight", val: ""}],
            gender: [{name: 'Мужчина', data: 'men'}, {name: "Женщина", data: 'women'}],
            currentGender: 'Стать',
            result: 0
        };
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
        this.handleGender = this.handleGender.bind(this);
    }

    changeHandler = (e) => {
        let newItems = this.state.parameters.map(item => item.name === e.target.dataset.name
            ? {...item, val: e.target.value}
            : item);
        this.setState({
                parameters: newItems
            }
        );
        e.target.value.length >= 1 && e.target.classList.remove('input-mistake');
    };

    validate = (e) => {
        const [weight] = this.state.parameters;
        let elems = document.querySelectorAll('input');
        if (!weight.val || isNaN(weight.val)) {
            elems[0].classList.add('input-mistake')
        }
        if (!isNaN(weight.val) && weight.val && this.state.currentGender !== "Стать") {
            return true
        }
    };

    submitHandler = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const [weight] = this.state.parameters;
           if (this.state.currentGender === "Женщина") {
               this.setState({
                   ...this.state,
                   result: parseInt(weight.val) * 30
               })
           }
           if(this.state.currentGender === "Мужчина"){
               this.setState({
                   ...this.state,
                   result: parseInt(weight.val) * 40
               })

           }

        }
    };
    handleGender = (e) => {
        this.setState({
            ...this.state,
            currentGender: e.target.innerHTML
        })
    };

    render() {
        return (
            <div className={`col-8 ${s.wrapper}`}>
                {
                    this.state.result === 0
                        ? [<div>
                        <BackLink/>
                            <p className={s.title}>Расчет физиологической потребности в жидкости</p>
                            <GenderParametr currentGender={this.state.currentGender} gender={this.state.gender}
                                            handleGender={this.handleGender.bind(this)}

                            />
                            <p className={s.text}>Эта формула исходит из тех физиологических потерь, которые несёт организм
                                в течение дня на жизнеобеспечение. Речь идёт о потреблении чистой питьевой воды.</p>
                            <FormCalculator parameters={this.state.parameters}
                                            submitHandler={this.submitHandler.bind(this)}
                                            changeHandler={this.changeHandler.bind(this)}

                            />
                        </div>]
                        : <NeededLiquidResults results={this.state.result}/>

                }

            </div>
        )
    }
}
