import React from "react";
import s from './neededliquidResults.module.scss';
import BackLink from "../../common/backLink/BackLink";

export default class NeededLiquidResults extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        let liquidNeeded = this.props.results/1000;
        return (
            <div className={s.wrapper}>
                <BackLink/>
                <p className={s.title}>Формула исходит из тех физиологических потерь, которые несёт организм в течение дня на жизнеобеспечение.</p>
                <p className={s.text}>Норма потребления воды: <span className={s.additionalText}>{liquidNeeded}л</span></p>
            </div>
        )
    }
}
