import React from "react";
import {Link} from 'react-router-dom';
import s from './wasteOfCalories.module.scss';

export default class WasteOfCalories extends React.Component {

    render() {
        return (
            <Link to='/metabolism' className={s.wrapper}>
                <p className={s.text}>Калькулятор расхода калорий при ходьбе</p>
            </Link>
        )
    }
}
