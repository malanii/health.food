import React from "react";
import s from './wasteOfCaloriesCalculator.module.scss';
import WasteOfCaloriesResults from "../wasteOfCaloriesResults/WasteOfCaloriesResults";
import FormCalculator from "../../common/form/FormCalculator";
import BackLink from "../../common/backLink/BackLink";


export default class WasteOfCaloriesCalculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            parameters: [{field: 'Рост, см', name: "height", val: ""}, {field: 'Вес, кг', name: "weight", val: ""},
                {field: 'Время ходьбы, мин', name: "time", val: ""}, {
                    field: 'Пройденное расстояние, м',
                    name: "distance",
                    val: ""
                }],
            currentGender: 'Стать',
            speed: 0,
            ccalPerMin: 0,
            fullWasteOfCCal: 0,
        };
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandler = this.changeHandler.bind(this);
    }

    changeHandler = (e) => {
        let newItems = this.state.parameters.map(item => item.name === e.target.dataset.name
            ? {...item, val: e.target.value}
            : item);
        this.setState({
                parameters: newItems
            }
        );
        e.target.value.length >= 1 && e.target.classList.remove('input-mistake');
    };

    validate = (e) => {
        const [height, weight, time, distance] = this.state.parameters;
        let elems = document.querySelectorAll('input');
        if (!height.val || isNaN(height.val)) {
            elems[0].classList.add('input-mistake')
        }
        if (!weight.val || isNaN(weight.val)) {
            elems[1].classList.add('input-mistake')
        }
        if (!time.val || isNaN(time.val)) {
            elems[2].classList.add('input-mistake')
        }
        if (!distance.val || isNaN(distance.val)) {
            elems[3].classList.add('input-mistake')
        }
        if ((!isNaN(height.val) && height.val) && (!isNaN(weight.val) && weight.val) && (!isNaN(time.val) && time.val) || (!isNaN(distance.val) && distance.val)) {
            return true
        }
    };

    submitHandler = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const [height, weight, time, distance] = this.state.parameters;
            let metrToKm = parseInt(distance.val) / 1000;
            let minToHours = parseInt(time.val) / 60;
            let v = metrToKm / minToHours;
            let m = parseInt(weight.val);
            let h = parseInt(height.val);
            let waste = Math.round(0.035 * m + (v * v / h) * 0.029 * m);
            let wasteFull = Math.round(parseInt(time.val) * waste)
            this.setState({
                speed: v,
                ccalPerMin: waste,
                fullWasteOfCCal: wasteFull
            });
        }
    };

    render() {
        return (
            <div className={`col-8 ${s.wrapper}`}>

                {
                    this.state.ccalPerMin === 0
                        ?[ <div>
                        <BackLink/>
                            <p className={s.title}>Калькулятор расхода калорий при ходьбе</p>
                            <FormCalculator parameters={this.state.parameters}
                                            submitHandler={this.submitHandler.bind(this)}
                                            changeHandler={this.changeHandler.bind(this)}

                            />
                        </div> ]
                        : <WasteOfCaloriesResults ccalPerMin={this.state.ccalPerMin}
                                                  fullWasteOfCCal={this.state.fullWasteOfCCal}
                                                  speed={this.state.speed}
                        />

                }

            </div>
        )
    }
}
