import React from "react";
import s from './wasteOfCaloriesResults.module.scss';
import BackLink from "../../common/backLink/BackLink";

export default class WasteOfCaloriesResults extends React.Component {
constructor(props){
    super(props);
}
    render() {
        return (
            <div className={s.wrapper}>
                <BackLink/>
                <p className={s.text}>Расход энергии: <span className={s.additionalText}>{this.props.ccalPerMin}ккал/мин</span></p>
                <p className={s.text}>Общее количество сожженных калорий: <span className={s.additionalText}>{this.props.fullWasteOfCCal}ккал</span></p>
                <p className={s.text}>Скорость ходьбы составила: <span className={s.additionalText}>{this.props.ccalPerMin}км/час</span></p>
            </div>
        )
    }
}
