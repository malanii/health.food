import React, {Component} from 'react';
import s from './content.module.scss';
import Title from "../common/text/title/Title";
import Description from "../common/text/description/Description";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlayCircle} from '@fortawesome/free-solid-svg-icons';
import LinkButton from "../common/button/linkButton/LinkButton";


export default class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            button: {
                link: '/recipes',
                name: 'Узнать больше',
                size: 'big'
            },
            title: 'Еда — важная часть сбалансированной диеты.',
            description: ["Ищите лучшие рецепты, выбирая сложность приготовления, и время приема пищи.",
                "А в дополнительных фильтрах можно искать по любимому ингредиенту: просто начните писать его название и сайт подберет соответствующий.",
                "Так же мы ждем Вас на нашем ютуб канале."]
        }
    }

    render() {

        return (
            <div className={s.wrapper}>
                <img className={s.img} src='./img/details/content.jpg' alt="home"/>

                <div className={`${s.container} `} >
                    <Title name={this.state.title}/>
                    {this.state.description.map((p) =>
                        <Description name={p}/>
                    )}

                    <div className={s.buttonContainer}>
                        <LinkButton {...this.state.button} />
                        <a href='https://www.youtube.com/channel/UCIEv3lZ_tNXHzL3ox-_uUGQ'
                           className={s.iconContainer}>
                            <FontAwesomeIcon className={s.icon} icon={faPlayCircle}/>
                            Смотреть видео
                        </a>

                    </div>
                </div>
            </div>
        )
    }
}
