import React, {Component} from 'react';
import s from './questionForm.module.scss'
import axios from "axios";
import validator from 'validator';
import QuestionsSuccess from "../questionsSuccess/QuestionsSuccess";


export default class QuestionForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: ['Имя и фамилия:', 'Ел.почта:', "Вопрос:"],
            formParam: [{name: "customerName", type: "normal", val: ""}, {name: "email", type: "normal", val: ""},
                {name: "question", type: "special", val: ""}],
            questionSent: false,
        };
    }

    changeHandler = (e) => {

        let newItems = this.state.formParam.map(item => item.name === e.target.dataset.name
            ? {...item, val: e.target.value}
            : item);
        this.setState({
                formParam: newItems
            }
        );
        e.target.value.length >= 1 && e.target.classList.remove('input-mistake')
    };

    validate = (e) => {
        const [customerName, email, question] = this.state.formParam;
        let elems = document.querySelectorAll('input');
        let elemsTextarea = document.querySelector('textarea');

        if (!customerName.val) {
            elems[0].classList.add('input-mistake')
        }
        if (!(validator.isEmail(email.val))) {
            elems[1].classList.add('input-mistake')
        }
        if (!question.val) {
            elemsTextarea.classList.add('input-mistake')
        } else {
            return true
        }
    };

    submitHandler = (e) => {
        e.preventDefault();
        const isValid = this.validate();
        if (isValid) {
            const [customerName, email, question] = this.state.formParam;
            let newQuestion = {
                name: customerName.val,
                emailUser: email.val,
                question: question.val
            };
            axios.post('/questions', newQuestion)
                .then(res => {

                        let newItems = this.state.formParam.map(item => item.val
                            ? {...item, val: ''}
                            : item);

                        this.setState({
                                formParam: newItems,
                                questionSent: true
                            }
                        );
                        console.log(this.state)
                    }
                )
                .catch(error =>
                    console.log(error)
                );
        }
    };

    render() {
        const {fields, formParam} = this.state;
        return (
            <div className='py-5'>
                {!this.state.questionSent
                ? [<form onSubmit={this.submitHandler} className={`col-9 col-sm-8 col-md-6 col-lg-6 col-xl-5 ${s.wrapper}`}>
                    <p className={s.title}>Связаться с нами</p>
                    <div className='d-flex justify-content-center'>
                        <div className='col-5'>
                            {fields.map(field =>
                                <p className={s.text}>{field}</p>
                            )}
                        </div>
                        <div className='col-5'>
                            {formParam.map(e => e.type === 'normal'
                                ? <input className={` questions-input questions-input-${e.type}`} type='text'
                                         data-name={e.name}
                                         value={e.val}
                                         onChange={this.changeHandler}/>
                                : <textarea className={` questions-input questions-input-${e.type}`} data-name={e.name}
                                            value={e.val}
                                            onChange={this.changeHandler}/>
                            )}
                        </div>
                    </div>
                    <div className={s.buttonWrapper}>
                        <button type='submit' className=' link-button link-button-big m-auto'>Отправить</button>
                    </div>
                </form>]
                    : <QuestionsSuccess/>
                }

            </div>
        );
    }
}