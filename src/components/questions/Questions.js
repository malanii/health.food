import React, {Component} from 'react';
import s from './questions.module.scss';
import Title from "../common/text/title/Title";
import LinkButton from "../common/button/linkButton/LinkButton";

export default class Questions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            button: {
                link:'/question',
                name: 'Задать вопрос',
                size: 'big'
            },
            title: 'Все еще остались вопросы?',
        }
    }
    render() {
        return (
            <div className={s.container}>
                <Title name={this.state.title}/>
                <div className={`${s.linkWrapper}`}>
                    <LinkButton {...this.state.button}/>
                </div>

            </div>
        )
    }
}