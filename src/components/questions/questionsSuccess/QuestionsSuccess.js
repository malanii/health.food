import React, {Component} from 'react';
import s from './questionSuccess.module.scss'

import {Link} from "react-router-dom";


export default class QuestionsSuccess extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <div className={s.wrapper}>
                <p className={s.text}>Спасибо! </p>
                <p className={s.text}>Наш менеджер скоро свяжется с Вами</p>
                <Link to='/' >
                    <p className={s.additionalText}>Вернутся на главную</p>
                </Link>
                <img className={s.img} src="/img/details/questionSuccess.png"/>
            </div>
        );
    }
}

