import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import {faWeight} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import s from './resultscard.module.scss'

export default class ResultCard extends React.Component {
constructor(props){
    super(props)
}
    render() {
        return (
            <div className={s.card}>
                <div className={s.front}>
                    <div className='d-flex'>
                        <img className={`${s.img} ${s.imgBefore}`} src={this.props.imgBefore}/>
                        <img className={`${s.img} ${s.imgAfter}`} src={this.props.imgAfter}/>
                    </div>
                    <p className={s.title}>{this.props.name}</p>
                    <div className={s.infoWrapper}>
                        <div className={s.text}>
                            {this.props.results}
                        </div>
                        <div className='p-2'>
                            <p className={`${s.text}`}>
                                <FontAwesomeIcon className={s.icon} icon={faWeight}/>
                                {this.props.sizes}
                            </p>
                        </div>
                    </div>
                </div>
                <div className={s.back}>
                    <img className={s.backImg} src='/img/details/logo.png'/>
                    <p className={`col-11 m-auto ${s.text}`}>"{this.props.story}"</p>
                </div>
            </div>
        );
    }
};
