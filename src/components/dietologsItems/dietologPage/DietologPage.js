import React, {Component} from 'react';
import s from './dietologPage.module.scss';
import StarRating from "../../common/starRating/StarRating";
import connect from "react-redux/es/connect/connect";
import {addToCartDietolog} from "../../../store/actions/cartAction";

export class DietologPage extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = (e) => {
        let currentDietolog = this.props;
        const {itemsProgram, itemsDietologs} = this.props.cart;
        if (itemsDietologs.length === 0) {
            this.props.addToCartDietolog(e, currentDietolog);
        } else {
            let existed_item = itemsDietologs.find(item => item.itemNo === currentDietolog.itemNo);
            existed_item
                ? console.log('est uzhe v korzine')
                : this.props.addToCartDietolog(e, currentDietolog)
        }
    };

    render() {
        return (
            <div key={this.props._id} className='col-12 '>
                <div className={`${s.header} col-8 flex-center`}>

                    <img src={this.props.imageUrls} className={`${s.img} col-12 col-lg-4`}/>

                    <div className='col-12 col-lg-8 '>
                        <div className='col-12 flex-between py-3  flex-column flex-md-row'>

                            <div className='col-10 col-md-7 text-center'>
                                <p className={s.name}>{this.props.name}</p>

                                <p className={s.textMain}>{this.props.specialization}</p>
                                <p className={s.textAdditional}>{this.props.qualification}</p>
                                <StarRating rating={Math.round(this.props.rating)}/>
                                <p className={s.textAdditional}>Опыт работы(года): <span>{this.props.experience}</span>
                                </p>
                            </div>

                            <div className='col-10 col-md-5 mb-3'>
                                <div className={`${s.consultationInfoWrapper} col-12`}>
                                    <p className={s.title}>Консультация:</p>
                                    <p className={s.textMain}>Цена: <span>{this.props.currentPrice} грн</span></p>
                                    <p className={s.textAdditional}>Длительность: <span>45 мин</span>
                                    </p>
                                </div>
                                <button onClick={(e) => {
                                    this.handleClick(e)
                                }} className='program-button-buy link-button-small link-button m-auto'>Заказать
                                </button>

                            </div>
                        </div>

                        <p className={s.textMain}>Дата и время консультации согласовуется с менеджером по
                            телефону</p>
                    </div>
                </div>


                <div className={`${s.educationWrapper} col-8 `}>

                    <p className={s.title}>Образование:</p>
                    <ul className={s.educationItem}>
                        {this.props.study.map(item => {
                            return (<li className={s.consultationText}>{item}</li>)
                        })}
                    </ul>

                    <p className={s.title}>Достижения:</p>
                    <ul className={s.educationItem}>
                        {this.props.progress.map(item => {
                            return (<li className={s.consultationText}>{item}</li>)
                        })}
                    </ul>

                    <p className={s.title}>Лечение заболеваний:</p>
                    <ul className={s.educationItem}>
                        {this.props.treatment.map(item => {
                            return (<li className={s.consultationText}>{item}</li>)
                        })}
                    </ul>

                    <p className={s.title}>Выполняемые процедуры:</p>
                    <ul className={s.educationItem}>
                        {this.props.procedures.map(item => {
                            return (<li className={s.consultationText}>{item}</li>)
                        })}
                    </ul>

                </div>
            </div>

        )
    }
};

const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    };
};
export default connect(mapStateToProps, {addToCartDietolog})(DietologPage);