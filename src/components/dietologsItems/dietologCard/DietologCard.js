import React, {Component} from 'react';
import s from './dietologCard.module.scss';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';
import 'bootstrap/dist/css/bootstrap.css';

export default class DietologCard extends React.Component {
    render() {
        return (
            <div key={this.props._id} className={s.wrapper}>
                <div className={s.header}>
                    <img className={s.img} src={this.props.imageUrls}/>
                    <div className={`${s.priceWrapper} flex-center`}>
                        <p className={`${s.name} ${s.margin}`}>{this.props.currentPrice}</p>
                        <img className={s.priceImg} src='/img/details/uah.png' alt='uah'/>
                    </div>
                </div>
                <div className={s.ratingContainer}>
                    <div className='flex-between-align'>
                        <img className={s.ratingImg} src='./img/details/ratingReceipt.png'/>
                        <p className={s.ratingText}>{this.props.rating}</p>
                    </div>
                    <p className={s.text}>{this.props.specialization} </p>
                </div>
                <p className={s.name}>{this.props.name}</p>
                <p className={s.additionText}>{this.props.qualification}</p>
                <Link className={s.button} to={`/dietologs/${this.props.itemNo}`}>
                    <FontAwesomeIcon className={s.buttonIcon} icon={faAngleDoubleRight}/>
                </Link>
            </div>
        )
    }
}
