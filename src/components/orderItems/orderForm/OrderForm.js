import React, {Component} from 'react';
import s from './orderForm.module.scss'

export default class OrderForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
        }
    }
    handleCheckboxChange = (e) => {
        // console.log('props new', this.props.formParam);
        this.setState({
            checked: e.target.checked
        });
    };
    render() {
        const {fields, placeOrder, formParam, changeHandler,handleCheckboxChange,checked} = this.props;
        return (
            <div className={`${s.container}  col-8`}>

                <form onSubmit={placeOrder} >
                    <div className='flex-around'>
                        <div className='col-5'>
                            {fields.map(field =>
                                <p className={s.text}>{field}</p>
                            )}
                        </div>
                        <div className='col-7'>
                            {formParam.map(e =>
                                <input className={s.input} type='text' data-name={e.name}
                                       value={e.val.value}
                                       onChange={(e) => {
                                           changeHandler(e)
                                       }}
                                />)}
                        </div>

                    </div>



                    <div className={`${s.checked} col-12`}>
                        <input onChange={handleCheckboxChange} checked={checked}
                               className={`${s.checkBox} col-1`} type="checkbox"/>
                        <p className={`${s.additionText} col-11`}>Оформляя заказ, Я
                            соглашаюсь
                            с условиями использования. Подтверждаю,
                            что проконсультировался с врачом и не имею противопоказаний</p>
                    </div>

                    <button type='submit' className={`link-button link-button-big ${s.button}`}> Оформить</button>
                </form>

            </div>
        );
    }
}

