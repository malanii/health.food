import React, {Component} from 'react';
import {connect} from "react-redux";
import s from './orderedItems.module.scss'
import {Link} from 'react-router-dom';

class OrderedItems extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        const {total, itemsProgram, itemsDietologs} = this.props.order;

        return (
            <div className={`${s.wrapper} col-4`}>
                {itemsDietologs && [
                    <div className='flex-between'>
                        <p className={s.title}>Консультация с диетологом</p>
                        <p className={s.text}> 45 минут</p>
                    </div>
                ]}

                <div className=''>
                    {itemsProgram.map(item =>
                        <div className='flex-between'>
                            <p className={s.title}>{item.name}</p>
                            <p className={s.text}>{item.quantity} <span>дн</span></p>
                        </div>
                    )}
                </div>

                <div className={s.total}>
                    <p className={s.text}>Итого:</p>
                    <p className={s.text}>{total}грн</p>
                </div>
               <Link className={s.additionalText} to='cart'>Редактировать заказ</Link>
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        order: state.order
    };
};
export default connect(mapStateToProps)(OrderedItems);