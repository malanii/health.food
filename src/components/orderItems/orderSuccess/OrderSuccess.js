import React, {Component} from 'react';
import s from './orderSuccess.module.scss';

export default class OrderSuccess extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div className={s.wrapper}>
                <img className={s.img} src="/img/details/orderedBack.jpg"/>
                <div className={s.container}>
                    <p className={s.text}>Спасибо!</p>
                    <p className={s.text}>Номер вашего заказа: </p>
                    <p className={s.text}>{this.props.orderNo}</p>
                </div>
            </div>
        );
    }
}

