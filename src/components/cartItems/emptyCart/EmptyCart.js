import React, {Component} from 'react';
import s from './emptyCart.module.scss';
import {Link} from "react-router-dom";

export default class EmptyCart extends React.Component {
    render() {
        return (
            <div className={`${s.wrapper} col-10`}>
                <img className={s.img} src='/img/details/emptyCart.jpg' alt="cart" />
                <p  className={s.text}>Корзина пуста</p>
                <p  className={s.text}>но это никогда не поздно исправить :)</p>
                <Link to='programs'><p className={s.link}>Перейти к программам питания</p></Link>
            </div>
        )
    }
}