import React from "react";
import s from './CartDietologCard.module.scss'
import {removeDietolog} from "../../../store/actions/cartAction";
import connect from "react-redux/es/connect/connect";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";

class CartDietologCard extends React.Component {
    constructor(props) {
        super(props);

    }
    removeDietolog = (e, itemNo) => {
        this.props.removeDietolog(e, itemNo);
    };

    render() {
        return (
            <div>
                {this.props.cart.itemsDietologs.map(item =>

                    <div className={`${s.wrapper} col-10 flex-column flex-md-row`}>
                        <Link className='default-padding col-3' to={`/dietologs/${item.itemNo}`}>
                            <img src={item.imageUrls} className={s.img}/>
                        </Link>

                        <div className='col-12 col-md-9'>
                            <p className={s.title}>Консультация с диетологом:</p>

                            <div className={`${s.contentContainer} flex-between-align`}>
                                <p className={`${s.name} col-3`}>{item.name}</p>
                                <div>
                                    <p className={s.detail}>Стоимость:</p>
                                    <div className='flex-between-align'>
                                        <p className={s.text}>{item.currentPrice}</p>
                                        <img className={s.priceImg} src='/img/details/uah.png' alt='uah'/>
                                    </div>
                                </div>

                                <div>
                                    <p className={s.detail}>Длительность консультации:</p>
                                    <p className={s.text}>45 мин</p>
                                </div>
                                <button className={`default-button ${s.button}`} onClick={(e) => {
                                    this.removeDietolog(e, item.itemNo)
                                }}><FontAwesomeIcon className={s.icon} icon={faTrash}/></button>
                            </div>

                            <p className={s.info}>дата и время консультации будет согласовано с
                                менеджером по телефону</p>

                        </div>

                    </div>
                )}
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        removeDietolog: (e, itemNo) => {
            dispatch(removeDietolog(e, itemNo))
        },

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CartDietologCard);

