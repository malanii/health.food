import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect";
import s from './CartProgramCard.module.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {decreaseQuantity, increaseQuantity, removeProgram} from "../../../store/actions/cartAction";
import {placeOrder} from "../../../store/actions/placeOrderAction";
import {Link} from "react-router-dom";

class CartProgramCard extends React.Component {
    constructor(props) {
        super(props);
    }
    removeProgram = (e, itemNo) => {
        this.props.removeProgram(e, itemNo);
    };
    handleDecreaseQuantity = (e, item) => {
        item.quantity > item.minQuantity && this.props.decreaseQuantity(e, item.itemNo)
    };
    handleIncreaseQuantity = (e, item) => {
        item.quantity < item.maxQuantity && this.props.increaseQuantity(e, item.itemNo)
    };
    render() {
        return(
            <div>
                {this.props.cart.itemsProgram.map(item =>
                    <div className={`${s.wrapper} flex-between-align col-10 flex-column flex-md-row`}>

                        <Link className='col-3 default-padding' to={`/programs/${item.itemNo}`}>
                        <img src={item.imageUrls} className={s.img}/>
                        </Link>

                        <div  className='col-12 col-md-9'>
                            <p className={s.title}>Программа питания:</p>

                            <div className={`${s.contentContainer} flex-between-align`}>
                                <p className={s.name}>{item.name}</p>

                                <div>
                                    <p className={s.detail}>Стоимость:</p>
                                    <div className='flex-between-align'>
                                        <p className={s.text}>{item.currentPrice}</p>
                                        <img className={s.priceImg} src='/img/details/uah.png' alt='uah'/>
                                    </div>
                                </div>

                                <div className='text-center'>
                                    <p className={s.detail}>Количество дней:</p>
                                    <div className='flex-center'>
                                        <button  className={`default-button ${s.button}`} onClick={(e) => {this.handleDecreaseQuantity(e, item)}}>-</button>
                                        <p className={s.text}>{item.quantity}</p>
                                        <button className={`default-button ${s.button}`} onClick={(e) => {this.handleIncreaseQuantity(e, item)}}>+</button>
                                    </div>
                                </div>
                                <button className={`default-button ${s.button}`} onClick={(e) => {this.removeProgram(e, item.itemNo)}}>
                                    <FontAwesomeIcon className={s.icon} icon={faTrash}/>
                                </button>
                            </div>


                        </div>


                    </div>

                )}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    }
};
const mapDispatchToProps = (dispatch) => {
    return {
        removeProgram: (e, itemNo) => {
            dispatch(removeProgram(e, itemNo))
        },
        decreaseQuantity: (e, itemNo) => {
            dispatch(decreaseQuantity(e, itemNo))
        },
        increaseQuantity: (e, itemNo) => {
            dispatch(increaseQuantity(e, itemNo))
        },
        placeOrder: (e, total, itemsProgram, itemsDietologs) => {
            dispatch(placeOrder(e, total, itemsProgram, itemsDietologs))
        }
    }
};
export default connect(mapStateToProps,mapDispatchToProps)(CartProgramCard);