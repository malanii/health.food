import React, {Component} from 'react';
import s from './advantages.module.scss';
import {faServer, faConciergeBell, faArrowCircleUp} from '@fortawesome/free-solid-svg-icons';
import Card from "../common/card/Card";
import Title from "../common/text/title/Title";
export default class Advantages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            card:[{
                title: 'Разнообразное меню',
                description: 'даже самый изысканный соискатель найдет диету по вкусу',
                size: "regular",
                icon: faServer,
            },
                {
                    title: 'Обновление рецептов',
                    description: 'обновления рецептов каждый понедельник',
                    size: "s.margin",
                    icon: faConciergeBell,
                },
                {
                    title: 'ТОП експерты',
                    description: 'мы собрали лучших экспертов для ваших быстрых результатов',
                    size: "regular",
                    icon: faArrowCircleUp,
                },
            ],
            title: 'Почему мы лучшие?'
        }
    }


    render() {
        return (
            <div className={s.wrapper}>
                <Title name={this.state.title}/>
                <div className={s.cardsContainer}>
                    {this.state.card.map((e) =>
                        <Card title={e.title} description={e.description} size={e.size} icon={e.icon}/>
                    )}
                </div>
            </div>

        )
    }
}
