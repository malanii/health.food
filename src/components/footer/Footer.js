import React, {Component} from 'react';
import s from './footer.module.scss';
import {NavLink} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faYoutube, faFacebook, faTelegram, faInstagram} from '@fortawesome/free-brands-svg-icons';


export default class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            footerData: [
                {name: "Программы", path: "/programs"},
                {name: "Диетологи", path: "/dietologs"},
                {name: "Рецепты", path: "/recipes"},
                {name: "Помощь", path: "/question"},
            ],
            footerSocialMedia: [faFacebook, faYoutube, faTelegram, faInstagram],
            contactsData: ["тел. 050-111-11-11", "info@healthfood.com", "г.Киев, пр.Науки 18"]
        }
    }

    render() {
        const footer = this.state.footerData.map((el) =>
            <NavLink className={s.link} to={el.path}>
                <li className={s.item}>{el.name}</li>
            </NavLink>
        );
        return (
            <div className={s.container}>
                <div className={s.row}>
                    <NavLink  exact to="/"><img className={s.logo} src='/img/details/logo.png' alt="logo"/></NavLink>

                    <ul className={s.nav}>
                        {footer}
                    </ul>
                </div>

                <div className={s.row}>
                    <div>
                        {this.state.footerSocialMedia.map(icon =>
                            <FontAwesomeIcon className={s.icon} icon={icon}/>
                        )}
                    </div>
                    <div>
                        {this.state.contactsData.map(el =>
                            <p className={s.text}>{el}</p>)}
                    </div>
                </div>

                <p className={s.text}>2020 all rights reserved health.food</p>
            </div>
        )
    }

}
