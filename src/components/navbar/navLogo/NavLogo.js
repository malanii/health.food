import React from 'react';
import {Link} from "react-router-dom";
import s from './navLogo.module.scss'
export default class NavLogo extends React.Component {
    render() {
        return (
            <Link exact to='/' className={s.wrapper}><img src='/img/details/logo.png' alt="logo" /></Link>
        )
    }
}