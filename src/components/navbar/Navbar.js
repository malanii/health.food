import React from 'react';
import NavMenu from './navMenu/NavMenu';
import NavLogo from './navLogo/NavLogo';
import NavCart from './navCart/NavCart';
import s from './Navbar.module.scss'
import Calculator from "./NavCalculator/NavCalculator";

export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {windowWidth: window.innerWidth};
    }

    handleResize = (e) => {
        {
            this.setState({windowWidth: window.innerWidth});
        }
    };

    componentDidMount() {
        window.addEventListener("resize", this.handleResize);
    }

    componentDidUpdate() {
        window.addEventListener("resize", this.handleResize);
    }

    render() {
        const {windowWidth} = this.state;

        if (windowWidth >= 997) {
            return (
                <div className={s.wrapper}>
                    <NavLogo className='col-2'/>
                    <div className={`col-10 ${s.container} d-flex justify-content-end `}>
                        <NavMenu className=' col-10'/>
                        <NavCart className='col-1 '/>
                        <Calculator className='col-1 '/>
                    </div>
                </div>
            )
        }

        if (windowWidth < 997) {
            return (
                <div className={s.wrapper}>
                    <NavMenu className='col-4'/>
                    <NavLogo className='col-4'/>
                    <NavCart className='col-4'/>
                </div>
            )
        }

    }
}