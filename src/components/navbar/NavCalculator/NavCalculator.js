import React from "react";
import {NavLink} from 'react-router-dom';
import s from './navCalculator.module.scss'

export default class NavCalculator extends React.Component {

    render() {
        return (
            <NavLink className={s.wrapper}  exact to='/calculator'>
               <p className={s.text}>Калькулятор</p>
            </NavLink>
        )
    }
}
