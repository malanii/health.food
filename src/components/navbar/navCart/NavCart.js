import React, {Component} from 'react';
import s from './navCart.module.scss';
import {NavLink} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
export class NavCart extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const {itemsProgram, itemsDietologs} = this.props.cart;
        let quantity = itemsProgram.length + itemsDietologs.length;
        return (
            <NavLink to="/cart" activeClassName={s.active} className={s.container}>
                <img className={s.img} src='/img/details/cart.png'/>
                <div className={s.counter}>{quantity}</div>
            </NavLink>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        cart: state.cart
    }
};
export default connect(mapStateToProps)(NavCart)