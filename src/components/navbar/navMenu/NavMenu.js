import React, {SVGAttributes as prevState} from "react";
import {NavLink} from 'react-router-dom';
import './navMenu.scss';
import onClickOutside from "react-onclickoutside";
import NavCalculator from "../NavCalculator/NavCalculator";

class NavMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            navMenu: [
                {name: 'Главная', path: "/", type: 'special'},
                {name: 'Программы', path: "/programs", type: 'normal'},
                {name: 'Диетологи', path: "/dietologs", type: 'normal'},
                {name: 'Рецепты', path: "/recipes", type: 'normal'},
                {name: ' До/После', path: "/results", type: 'normal'},
            ],
            visibility: false,
            windowWidth: window.innerWidth
        };

    }

    handleResize = (e) => {
        {
            this.setState({windowWidth: window.innerWidth});
        }
    };
    openMenu= (e) => {
        this.setState({
            visibility: true
        })
    };
    componentDidMount() {
        window.addEventListener("resize", this.handleResize);
    }

    componentDidUpdate() {
        window.addEventListener("resize", this.handleResize);
    }

    handleClickOutside = e => {
        if (this.state.windowWidth < 997) {
            this.setState({
                visibility: false
            })
        }
    };
   closeMenu = (e) => {
        if (e) {
            e.preventDefault();
        }
        this.setState({visibility: !this.state.visibility});
    };
    render() {
        if (this.state.windowWidth < 997) {
            if (this.state.visibility) {
                return (
                    <div>
                        <div onClick={this.openMenu}>
                            <div className='menu-hamburger'></div>
                            <div className='menu-hamburger'></div>
                            <div className='menu-hamburger'></div>
                        </div>
                        <ul className='nav-menu mr-4' data-name="menu"  onClick={this.closeMenu}>
                            <a href="#" onClick={this.closeMenu} className="close"></a>
                            {this.state.navMenu.map((el) =>
                                el.type = "special"
                                    ?
                                    <NavLink className='clear-default-li nav-link-wrapper'
                                             activeClassName={`active-nav-${el.type}`}
                                             exact
                                             to={el.path}
                                            >
                                        <li className='nav-item '>
                                            {el.name}
                                        </li>
                                    </NavLink>
                                    :
                                    <NavLink className='clear-default-li nav-link-wrapper'
                                             activeClassName={`active-nav-${el.type}`}
                                             to={el.path}
                                             >
                                        <li className='nav-item '>
                                            {el.name}
                                        </li>
                                    </NavLink>
                            )}
<NavCalculator/>
                        </ul>
                    </div>

                )
            }
            if (!this.state.visibility) {
                return (
                    <div onClick={this.openMenu}>
                        <div className='menu-hamburger'></div>
                        <div className='menu-hamburger'></div>
                        <div className='menu-hamburger'></div>
                    </div>

                )

            }

        }

        if (this.state.windowWidth >= 997) {

            return (
                <ul className='nav-menu mr-4' data-name="menu">
                    {this.state.navMenu.map((el) =>
                        el.type = "special"
                            ?
                            <NavLink className='clear-default-li nav-link-wrapper'
                                     activeClassName={`active-nav-${el.type}`}
                                     exact
                                     to={el.path}>
                                <li className='nav-item '>
                                    {el.name}
                                </li>
                            </NavLink>
                            :
                            <NavLink className='clear-default-li nav-link-wrapper'
                                     activeClassName={`active-nav-${el.type}`}
                                     to={el.path}>
                                <li className='nav-item '>
                                    {el.name}
                                </li>
                            </NavLink>
                    )}

                </ul>
            )
        }

    }
}

export default onClickOutside(NavMenu);