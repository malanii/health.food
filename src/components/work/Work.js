import React from 'react';
import s from './work.module.scss';
import Title from "../common/text/title/Title";
import Description from "../common/text/description/Description";

export default class Work extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            workDirectionData: [
                'Выберите программу согласно вашим целям и предпочтениям',
                'Закажите консультацию у нашего диетолога',
                'Добавьте в корзину понравившуюся программу и консультацию с диетологом',
                'Заходите к нам каждый понедельник и смотрите обновление рецептов'
            ],
            title: 'Как это работает?',
            description: "вы сможете сбросить лишние килограммы, очистить питание и приучить себя к разумному потреблению пищи."
        }
    }
    render() {
        return (
            <div className={s.container}>
                <Title name={this.state.title}/>
                <Description name={this.state.description}/>
                <img className={s.img} src='./img/details/work_line.png' alt="home"/>
                <div className='flex-around '>
                    {this.state.workDirectionData.map(el =>
                        <p className={`${s.text}`}>{el}</p>
                    )}
                </div>
            </div>
        )
    }
}