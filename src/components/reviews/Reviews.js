import React, {Component} from 'react';
import s from './reviews.module.scss';
import Title from "../common/text/title/Title";
import Description from "../common/text/description/Description";
import Carousel from "react-bootstrap/Carousel";
import 'bootstrap/dist/css/bootstrap.css';
import axios from "axios";

export default class ControlledCarousel extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            title : 'Что говорят о нас?',
            description: "более тысячи клиентов, которые уже изменили свою жизнь и готовы поделиться своим опытом",
            reviews: []
        }
    }
    componentDidMount() {
        axios.get(`/reviews`)
            .then(res => this.setState({
                reviews: res.data
            }));
    }

    render() {
        const {title, description, reviews} = this.state;
        const review = reviews.map(item =>
            <Carousel.Item>
                <div className={s.item}>

                    <img  src={item.image} alt="logo"/>

                    <p className={s.text}>
                        {item.review}</p>
                    <img src='/img/details/rating.png' alt="logo"/>
                </div>
            </Carousel.Item>
        );
        return (
            <div className={s.container}>
                <Title name={title}/>
                <Description name={description}/>
                <Carousel>
                    {review}
                </Carousel>
            </div>
        )


    }
}

