import React, {Component} from 'react';
import s from './preloader.module.scss'


export default class Preloader extends React.Component {
    render() {
        return (
            <div className={s.wrapper}>
                <img className={s.img} src='/img/details/loader_food.png'/>
                <img className={s.imgPan} src='/img/details/loader_pan.png'/>
                <p className={s.text}>Loading...</p>
            </div>
        )
    }

};

