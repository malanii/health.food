import React from "react";
import s from './card.module.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Link} from "react-router-dom";


export default class Card extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const size = this.props.size;

        return (
            <div className={s.card}>
                <div className={s.icon_wrapper}> <FontAwesomeIcon className={s.icon} icon={this.props.icon}/></div>
                <p className={s.title}>{this.props.title}</p>
                <p className={s.text}>{this.props.description}</p>
            </div>
        )
    }
}
