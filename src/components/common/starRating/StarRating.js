import React from "react";
 import Stars from 'simple-rating-stars';
const StarRating = (props) => (
    <Stars
        stars={props.rating}
        outOf={5}
        full={'#dfff00'}
        empty={'#E1F1FF'}
        stroke={'#fa9c22'}
    />

);
export default StarRating;





