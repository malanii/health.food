import React, {Component} from 'react';
import s from './paginator.module.scss';

export default class Paginator extends React.Component {
    constructor(props) {
        super(props)

    }
    render() {

        const {totalProgramsCount, pageSize, currentPage, onPageChange} = this.props;
        const pagesCount = Math.ceil(totalProgramsCount / pageSize);
        let pages = [];
        for (let i = 1; i <= pagesCount; i++) {
            pages.push(i)
        }
        return (
            <div className={s.wrapper}>
                {
                    pages.map(n =>
                        <button onClick={(e) => {
                            onPageChange(e, n)
                        }} className={currentPage === n ? `${s.text} ${s.active}` : `${s.text}`}>{n}</button>
                    )
                }
            </div>
        )
    }
}

