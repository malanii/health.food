import React from "react";
import './buttonSortFilter.scss'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faStream} from "@fortawesome/free-solid-svg-icons";
import Dropdown from "react-bootstrap/Dropdown";


export default class ButtonSortFilter extends React.Component {
    constructor(props) {
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibility: false,
        };
    }

    handleToggleVisibility() {
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility
            };
        });
    }

    render() {
        const arrayOfFiltersKeys = Object.values(this.props.filters);
        return (

            <div className='mt-5 '>
                <div className='pr-5 pl-5 d-flex justify-content-between'>
                    <div className='d-flex  sort-button align-items-center col-6'>
                        <p className='button-text'> сортировать по:</p>
                        <Dropdown>
                            <Dropdown.Toggle variant="success" id="dropdown-basic">
                                {this.props.activeSortParam}
                            </Dropdown.Toggle>
                            <Dropdown.Menu className='dropdown-sort'>
                                {this.props.buttonSort.map(button =>
                                    <Dropdown.Item
                                        onClick={(e) => {
                                            this.props.handleSort(e)
                                        }}
                                        data-sort={button.data}>{button.name}</Dropdown.Item>)
                                }
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>

                    <button className='btn-success-filter btn-success text-description '
                            onClick={this.handleToggleVisibility}>
                        <FontAwesomeIcon className='mr-2 filter-button-icon'
                                         icon={faStream}/> Фильтр
                    </button>
                </div>
                {this.state.visibility && (
                    <div className='d-flex'>
                        {
                            arrayOfFiltersKeys.map(el =>
                                <div className='col-4 filter-items-wrapper'>
                                    <div className=' d-flex justify-content-between'>
                                        <p className='mb-1 mr-2 text-description'>{el.filterName}</p>
                                        <button onClick={(e) => {
                                            this.props.deleteFilter(e)
                                        }} className='button-delete-filter text-description'>Очистить
                                        </button>
                                    </div>
                                    <Dropdown>
                                        <Dropdown.Toggle className='drop-test' variant="success"
                                                         id="dropdown-basic">
                                            {el.activeFilter}
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu className='drop-test' id={el.id}>
                                            {el.filtersParams.map(button =>
                                                <Dropdown.Item onClick={(e) => {
                                                    this.props.handleFilter(e)
                                                }} data-sort={button.data}>{button.name}
                                                </Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </div>
                            )
                        }
                    </div>

                )}

            </div>


        )
    }
}
