import React, {Component} from 'react';
import './linkButton.scss';
import {Link} from 'react-router-dom';



export default class LinkButton extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className='link-wrapper'>
                <Link className={`link-button-${this.props.size} basic-link-style link-button `}  to={this.props.link}>{this.props.name}</Link>
            </div>
        )
    }
}

