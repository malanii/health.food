import React, {Component} from 'react';
import s from '../noResultsFound.module.scss'
import {connect} from "react-redux";
import {recipesFetchData} from "../../../../store/actions/fetchDataRecipesAction";


class NoResultsFoundRecipes extends React.Component {
    constructor(props) {
        super(props);
    }

    clearFilter = (e) => {
        this.props.recipesFetchData(this.props.recipes.currentFilter)
    };

    render() {
        return (
            <div className={s.wrapper}>

                <p className={`${s.text} ${s.title}`}>К сожалению по вашему запросу ничего не найдено.</p>
                <p className={s.text}>Попробуйте изменить свой поисковой запрос</p>
                <button className={`${s.text} ${s.additionalText} default-button`} onClick={this.clearFilter}>Очистить поиск</button>
            </div>
        )
    }

};
const mapStateToProps = (state) => {
    return {
        recipes: state.recipes,
    };
};
export default connect(mapStateToProps, {recipesFetchData})(NoResultsFoundRecipes);