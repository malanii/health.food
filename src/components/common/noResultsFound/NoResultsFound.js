import React, {Component} from 'react';
import s from './noResultsFound.module.scss'





 export default class NoResultsFound extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={s.wrapper}>
                <p className={`${s.text} ${s.title}`}>К сожалению по вашему запросу ничего не найдено.</p>
                <p className={s.text}>Попробуйте изменить свой поисковой запрос</p>

            </div>
        )
    }

};
