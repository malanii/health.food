import React, {Component} from 'react';
import './description.scss';

export default class Description extends React.Component {
    render() {
        return (
            <p className='text-description'>{this.props.name}</p>
        )
    }
};
