import React, {Component} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import s from './findRecipes.module.scss'
import {connect} from "react-redux";
import axios from "axios";
import {searchRecipes} from "../../../store/actions/fetchDataRecipesAction";

// import onClickOutside from "react-onclickoutside";


class FindRecipes extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dropDown: ['авокадо', "капуста", "огурцы", "курица", "рис", "молоко", "свекла", "шпинат", 'яблоко'],
            name: "search",
            val: "",
            visibility: false,
        };
        this.findRecipes = this.findRecipes.bind(this);
        this.editProduct = this.editProduct.bind(this);
    }

    changeHandler = (e) => {
        this.setState({
                val: e.target.value,
                visibility: false
            }
        );
    };
    keyPressed = (e) => {
        if (e.key === "Enter") {
            this.findRecipes()
        }
    };
    showDropDown = (e) => {
        this.setState({visibility: true});
    };

    hideList = (e) => {
        console.log(e.target.closest(".container"))
    };


    // handleClickOutside = (e) => {
    //     this.setState({
    //         visibility: false
    //     })
    //
    // };

    editProduct = (e) => {
        this.setState({val: e.target.innerHTML});
        // this.findRecipes();
    };
    findRecipes = (e) => {
        if (this.state.val !== ' ') {
            const searchPhrases = {
                query: this.state.val
            };
            axios.post("/receipts/search", searchPhrases)
                .then(res => {
                    this.setState({
                            // val: " ",
                            visibility: false
                        }
                    );
                    this.props.searchRecipes(res.data)
                })
                .catch(error => {
                    console.log(error.response)
                });
        }

    };

    render() {
        const {name, val} = this.state;
        // console.log(this.state.val);
        return (
            <div data-name='productList' onClick={this.hideList} className={s.wrapper}>
                <input className={s.input}
                       data-name={name}
                       value={val}
                       onChange={this.changeHandler} onKeyPress={this.keyPressed}
                       onClick={this.showDropDown}
                />
                <FontAwesomeIcon onClick={this.findRecipes} className={s.icon} icon={faSearch}/>
                {
                    this.state.visibility
                    && [<div className={s.dropdown}>
                        {
                            this.state.dropDown.map(e =>
                                <p className={s.dropdownItem} onClick={this.editProduct}>{e}</p>
                            )
                        }
                    </div>]
                }

            </div>
        )
    }
}

export default connect(null, {searchRecipes})(FindRecipes);

