import React from "react";
import s from './receiptCard.module.scss';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faAngleDoubleRight} from '@fortawesome/free-solid-svg-icons';


export default class ReceiptCard extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div key={this.props._id} className={s.card}>
                <div className={s.header}>
                    <img className={s.img} src={this.props.imgDishes}/>
                    <p className={s.time}>{this.props.time}</p>

                    <div className={s.rating}>
                        <img className={s.ratingImg} src='./img/details/ratingReceipt.png'/>
                        <p className={s.title}>{this.props.rating}</p>
                    </div>
                </div>
                <div className={s.contentContainer}>
                    <p className={s.title}>{this.props.name}</p>
                    <p className={s.text}>{this.props.diet}</p>
                </div>
                <p className={s.additionalText}>{this.props.eating}</p>
                <Link className={s.button} to={`/recipes/${this.props.itemNo}`}><FontAwesomeIcon
                    className={s.icon} icon={faAngleDoubleRight}/></Link>
            </div>
        )
    }


};
