import React from "react";
import s from './receiptDirection.module.scss'
export default class ReceiptPageDirection extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={`col-8 ${s.wrapper}`}>
                <p className={s.text}>{this.props.direction}</p>
            </div>


        )
    }
}
