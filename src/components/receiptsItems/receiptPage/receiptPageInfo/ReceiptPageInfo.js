import React from "react";
import s from './receiptPageInfo.module.scss';
import StarRating from "../../../common/starRating/StarRating";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHistory} from "@fortawesome/free-solid-svg-icons";


export default class ReceiptPageInfo extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div key={this.props._id}
                 style={{backgroundImage: `url(${this.props.imgIngredient})`, backgroundSize: "cover"}}
                 className={`${s.wrapper}  col-9 `}>

                <div className={`${s.container}  col-12 col-sm-8 col-lg-7`}>
                    <p className={s.title}>{this.props.name}</p>
                    <StarRating rating={this.props.rating}/>
                    <p className={s.title}>"{this.props.diet}"</p>

                    <div className='flex-around-align pb-4'>

                        <div className='col-6 '>

                            <p className={s.text}>Сложность:
                                <span className='ml-2'>{`${this.props.difficulty}/10`}</span>
                            </p>
                            <p className={s.additionalText}>{this.props.eating}</p>
                            <p className={s.text}><FontAwesomeIcon className={s.icon} icon={faHistory}/>
                                <span className='ml-2'>{this.props.time}</span></p>
                        </div>

                        <div className="col-6">
                            <p className={s.ccalDescription}>{this.props.nutritionFacts}</p>
                        </div>
                    </div>


                </div>
            </div>
        )
    }
}
