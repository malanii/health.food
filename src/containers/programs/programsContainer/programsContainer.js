import React from 'react'
import s from './programsContainer.module.scss'
import ProgramsCard from "../../../components/programsItem/programsCard/ProgramsCard";
import {itemsFetchDataPrograms} from "../../../store/actions/fetchDataProgramsAction";
import {connect} from "react-redux";
import Preloader from "../../../components/common/preloader/Preloader";
import Paginator from "../../../components/common/Paginator/Paginator";
import ButtonSortFilter from "../../../components/common/button/buttonFilter/ButtonSortFilter";
import NoResultsFound from "../../../components/common/noResultsFound/NoResultsFound";
class ProgramsContainer extends React.Component {
    constructor(props) {
        super(props);
        this.onPageChange = this.onPageChange.bind(this);
        this.handleSort = this.handleSort.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.deleteFilter = this.deleteFilter.bind(this);
        this.state = {
            activeSortParam: 'цене',
            buttonSort: [
                {name: 'возрастанию цены', data: 'currentPrice'},
                {name: 'убыванию цены', data: '-currentPrice'}
            ],
            filters: {
                goals: {
                    activeFilter: 'Выбрать:',
                    id: 'goals',
                    filterName: "В зависимости от цели",
                    filtersParams: [
                        {name: 'похудение', data: 'loseWeight',},
                        {name: 'оздоровление', data: 'generalHealth'},
                        {name: 'сбалансированное питание', data: 'balance'},
                        {name: 'удержание сахара', data: 'balanceSugar'},
                        {name: 'набор мышечной массы', data: 'gainWeight'}
                    ],
                },
                target: {
                    activeFilter: 'Выбрать:',
                    id: 'target',
                    filterName: "Для кого",
                    filtersParams: [
                        {name: 'для всех', data: 'all',},
                        {name: 'для беременных', data: 'pregnant'},
                        {name: 'для спортсменов', data: 'sportsmen'},
                        {name: 'для вегетарианцев', data: 'vegetarian'},
                        {name: 'для диабетиков', data: 'diabetics'},
                        {name: 'при сердечно-сосудистых заболеваниях', data: 'heartDisease'}
                    ],
                }
            },

        };

    }

    onPageChange = (e, currentPageNew) => {
        this.props.programs.currentFilter.startPage = currentPageNew;

        this.props.itemsFetchDataPrograms(this.props.programs.currentFilter);
    };

    handleSort = (e) => {
        this.props.programs.currentFilter.sort = this.state.buttonSort
            .filter(item => item.data === e.target.dataset.sort)
            .map(item => item.data)
            .join();
        this.setState({
            activeSortParam: e.target.innerHTML
        });
        this.props.itemsFetchDataPrograms(this.props.programs.currentFilter);
    };
    handleFilter = (e) => {
        const {goals, target} = this.state.filters;
        if (e.target.closest('div').id === 'goals') {

            let currentGoals = goals.filtersParams
                .filter(item => item.data === e.target.dataset.sort)
                .map(item => item.name)
                .join();

            this.props.programs.currentFilter.goals = currentGoals;
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    goals: {
                        ...this.state.filters.goals,
                        activeFilter: currentGoals
                    }

                }
            });
        } else if (e.target.closest('div').id === 'target') {
            let currentTarget = target.filtersParams
                .filter(item => item.data === e.target.dataset.sort)
                .map(item => item.name)
                .join();

            this.props.programs.currentFilter.target = currentTarget;
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    target: {
                        ...this.state.filters.target,
                        activeFilter: currentTarget
                    }

                }
            });
        }
        this.props.itemsFetchDataPrograms(this.props.programs.currentFilter);
    };

    deleteFilter = (e) => {
        const {goals, target} = this.state.filters;
        let deleteFilter = e.target.parentNode.firstChild;
        if (deleteFilter.innerHTML === goals.filterName) {
            this.props.programs.currentFilter.goals = '';
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    goals: {
                        ...this.state.filters.goals,
                        activeFilter: 'Выбрать:'
                    }
                }
            });
        }
        else if (deleteFilter.innerHTML === target.filterName) {
            this.props.programs.currentFilter.target = '';
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    target: {
                        ...this.state.filters.target,
                        activeFilter: 'Выбрать:'
                    }

                }
            });
        }

        this.props.itemsFetchDataPrograms(this.props.programs.currentFilter);
    };

    componentDidMount() {
        this.props.itemsFetchDataPrograms(this.props.programs.currentFilter);
    }

    render() {
        const {items, loading, errors, currentFilter} = this.props.programs;
        const {totalProgramsCount, startPage, pageSize, sort, target, goals} = currentFilter;
        const renderPrograms = items.map((item) => {
            return (
                <ProgramsCard key={item._id} {...item}/>
            )
        });
        return (
            <div>
                <ButtonSortFilter activeSortParam={this.state.activeSortParam} buttonSort={this.state.buttonSort}
                                  filters={this.state.filters}
                                  handleSort={this.handleSort.bind(this)}
                                  handleFilter={this.handleFilter.bind(this)}
                                  deleteFilter={this.deleteFilter.bind(this)}/>
                <div className={s.container}>
                    {loading ? <Preloader/> : renderPrograms}
                    {(!loading && totalProgramsCount === 0) && <NoResultsFound/>}
                </div>
                    <Paginator totalProgramsCount={totalProgramsCount} pageSize={pageSize} currentPage={startPage}
                               onPageChange={this.onPageChange.bind(this)}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        programs: state.programs,
    };
};
export default connect(mapStateToProps, {itemsFetchDataPrograms})(ProgramsContainer);

