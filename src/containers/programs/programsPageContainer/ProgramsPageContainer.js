import React from 'react'
import axios from 'axios';
import ProgramsPageInfo from "../../../components/programsItem/programPage/programsPageInfo/ProgramsPageInfo";
import ProgramsPageNutrition
    from "../../../components/programsItem/programPage/programsPageNutrition/ProgramsPageNutrition";
import Preloader from "../../../components/common/preloader/Preloader";


class ProgramsPageContainer extends React.Component {
    state = {
        weeklyCount: [],
        fullProgram: [],
        loading: true
    };

    componentDidMount() {
        const receipts = this.props.match.params;
        const receiptNo = receipts.itemNo;
        axios.get(`/programs/${receiptNo}`)
            .then(rec => {
                this.setState(
                    {
                        fullProgram: rec.data
                    }
                );
                this.setState(
                    {
                        loading: false
                    }
                );
                const resultsReceipt = rec.data;
                this.setState(
                    {
                        weeklyCount: resultsReceipt.weeklyCount
                    }
                );
            });
    }

    render() {
        const {weeklyCount,fullProgram, loading} = this.state;
        return (
            <div>
                <div className='pt-5'>
                    {loading ? <Preloader/> : <ProgramsPageInfo {...fullProgram}/>}
                </div>
                <ProgramsPageNutrition weeklyCount={weeklyCount}/>
            </div>
        )
    }
}

export default ProgramsPageContainer;
