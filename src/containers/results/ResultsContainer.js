import React, {Component} from 'react'
import ResultCard from '../../components/resultsCard/ResultsCard'
import axios from 'axios'
import s from './resultsContainer.module.scss'
import Preloader from "../../components/common/preloader/Preloader";
import Paginator from "../../components/common/Paginator/Paginator";
export default class ResultsContainer extends React.Component {
    constructor() {
        super();
        this.state = {
            results: [],
            loading: true,
            pageSize: 6,
            totalProgramsCount: 0,
            currentPage: 1,
            errors: false,
        };
    }
    onPageChange = (e, currentPageNew) => {
        axios.get(`/results/filter?perPage=${this.state.pageSize}&startPage=${currentPageNew}`)
            .then(res => {
                const {results, resultsQuantity} = res.data;
                this.setState({
                    results: results,
                    totalProgramsCount: resultsQuantity,
                    loading: false,
                    currentPage: currentPageNew
                })
            })
    };
    componentDidMount() {
        axios.get(`/results/filter?perPage=${this.state.pageSize}&startPage=${this.state.currentPage}`)
            .then(res => {
                const {results, resultsQuantity} = res.data;
                this.setState({
                    results: results,
                    totalProgramsCount: resultsQuantity,
                    loading: false,
                })
            })
            .catch(error => {
                this.setState({
                    errors: true,
                });
                console.log(error.response)
            });
    }
    render() {
        const {results, loading, pageSize, totalProgramsCount, currentPage, errors} = this.state;
        const renderResults = results.map(result =>
            <ResultCard key={result._id} {...result}/>);
        return (
            <div>
                {loading ? <Preloader/> : [
                   <div>
                       <div className={s.container}>
                           {loading ? <Preloader/> : renderResults}
                       </div>
                           <Paginator totalProgramsCount={totalProgramsCount} pageSize={pageSize} currentPage={currentPage}
                                      onPageChange={this.onPageChange.bind(this)}/>
                   </div>
                ]}
            </div>
        )
    }
}

