import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import s from './cartContainer.module.scss'
import {Link} from 'react-router-dom';
import {placeOrder} from "../../store/actions/placeOrderAction";
import CartProgramCard from "../../components/cartItems/cartProgramCard/CartProgramCard";
import CartDietologCard from "../../components/cartItems/cartDietologCard/CartDietologCard";
import EmptyCart from "../../components/cartItems/emptyCart/EmptyCart";


class CartContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    placeOrder = (e, total, itemsProgram, itemsDietologs) => {
        const itemsProgramFull = itemsProgram.map(item => {
                const programs = {
                    name: item.name,
                    quantity: item.quantity
                };
                return programs
            }
        );
        const arrayOfDietologsName = itemsDietologs.map(item =>
            item.name
        );

        const dietologName = arrayOfDietologsName.join('');
        this.props.placeOrder(e, total, itemsProgramFull, dietologName);
    };


    render() {

        const {itemsProgram, itemsDietologs} = this.props.cart;
        const totalProgramsPrice = itemsProgram.map(program => program.currentPrice)
            .reduce((currentPrice, current) => currentPrice + current, 0);
        const totalDietologsPrice = itemsDietologs.map(dietolog => dietolog.currentPrice)
            .reduce((currentPrice, current) => currentPrice + current, 0);

        const total = totalProgramsPrice + totalDietologsPrice;

        return (
            <div className={s.container}>
                {
                    (itemsProgram.length === 0 && itemsDietologs.length === 0)
                        ? <EmptyCart/>
                        : [<div className={s.text}>
                            <CartProgramCard/>
                            <CartDietologCard/>
                            <div className={s.total}>
                                <p className={s.text}>Сумма заказа:</p>
                                <p className={`${s.text} ml-2`}>{total}</p>
                                <img className={s.priceImg} src='/img/details/uah.png' alt='uah'/>
                            </div>
                            <div className="link-wrapper col-6 col-sm-4 col-md-2 m-auto">
                                <Link onClick={(e) => {
                                    this.placeOrder(e, total, itemsProgram, itemsDietologs)
                                }} to="/order" className='link-button link-button-big'>Оформить заказ</Link>
                            </div>
                        </div>]
                }

            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        cart: state.cart,
    }
};
export default connect(mapStateToProps, {placeOrder})(CartContainer);

