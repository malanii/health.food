import React from 'react';
import Header from "../../components/header/Header";
import Navbar from "../../components/navbar/Navbar";
import Advanages from "../../components/advantages/Advantages";
import Content from "../../components/content/Content";
import Work from "../../components/work/Work";
import ControlledCarousel from "../../components/reviews/Reviews";
import Questions from "../../components/questions/Questions";

import ScrollToTop from "../../components/common/button/backToTopButton/ScrollToTop";


export default function HomePage() {
    return (
        <div className='main-container'>
                <Header/>
            <div className='main-background'>
                <Advanages/>
                <Content/>
                <Work/>
                <ControlledCarousel/>
                <Questions/>
            </div>
            <ScrollToTop/>
        </div>

    )

}
