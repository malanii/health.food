import React from 'react';
import '../App.scss';
import "../scss/style.scss"
import {Switch, Route} from 'react-router-dom';
import HomePage from "../containers/homepage/HomePage";
import DietologsContainer from "./dietologs/dietologsContainer/DietologsContainer";
import DietologPageContainer from "./dietologs/dietologsPageContainer/DietologPageContainer";
import ResultsContainer from "./results/ResultsContainer";
import ReceiptPageContainer from "./receipts/receiptPageContainer/ReceiptPageContainer";
import ReceiptsContainer from "./receipts/receiptsContainer/ReceiptsContainer";
import ProgramsContainer from "./programs/programsContainer/programsContainer";
import ProgramsPageContainer from "./programs/programsPageContainer/ProgramsPageContainer";
import QuestionForm from "../components/questions/questionForm/QuestionForm";
import RoutesToTop from "../routes/RoutesToTop";
import CartContainer from "./cart/CartContainer";
import Order from "./order/Order";
import Navbar from "../components/navbar/Navbar";
import Footer from "../components/footer/Footer";
import Calculator from "./calculator/Calculator";
import MetabolismCalculator from "../components/calculatorItems/wasteOfCalories/wasteOfCaloriesCalculator/WasteOfCaloriesCalculator";
import NeededLiquidCalculator from "../components/calculatorItems/neededLiquid/needeLiquidCalculator/NeededLiquidCalculator";
import BodyMassIndexCalculator
    from "../components/calculatorItems/bodyMassIndex/BodyMassIndexCalculator/BodyMassIndexCalculator";


function App() {

    return (
        <Switch>
            <div className='app-background'>
                <div className='main-container'>
                    <div className="navbar-content-wrapper">
                        <Navbar/>
                            <RoutesToTop/>
                            <Route exact path="/" component={HomePage}/>
                            <Route exact path="/programs" component={ProgramsContainer}/>
                            <Route exact path="/programs/:itemNo" component={ProgramsPageContainer}/>
                            <Route exact path="/recipes" component={ReceiptsContainer}/>
                            <Route exact path="/dietologs" component={DietologsContainer}/>
                            <Route exact path="/dietologs/:itemNo" component={DietologPageContainer}/>
                            <Route exact path="/results" component={ResultsContainer}/>
                            <Route exact path="/recipes/:itemNo" component={ReceiptPageContainer}/>
                            <Route exact path="/question" component={QuestionForm}/>
                            <Route exact path="/cart" component={CartContainer}/>
                            <Route exact path="/order" component={Order}/>
                            <Route exact path="/calculator" component={Calculator}/>
                            <Route exact path="/metabolism" component={MetabolismCalculator}/>
                            <Route exact path="/needed_liquid" component={NeededLiquidCalculator}/>
                            <Route exact path="/body_mass_index" component={BodyMassIndexCalculator}/>
                    </div>
                    <Footer/>
                </div>
            </div>
        </Switch>
    )
}

export default App;
