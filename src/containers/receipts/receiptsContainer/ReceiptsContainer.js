import React, {Component} from 'react'
import ReceiptCard from "../../../components/receiptsItems/receiptCard/ReceiptCard";
import s from './receiptsContainer.module.scss'
import Preloader from "../../../components/common/preloader/Preloader";
import {connect} from "react-redux";
import {recipesFetchData} from "../../../store/actions/fetchDataRecipesAction";
import Paginator from "../../../components/common/Paginator/Paginator";
import ButtonSortFilter from "../../../components/common/button/buttonFilter/ButtonSortFilter";
import FindRecipes from "../../../components/receiptsItems/findRecipes/FindRecipes";
import NoResultsFoundRecipes
    from "../../../components/common/noResultsFound/noResultsFoundRecipes/NoResultsFoundRecipes";

class ReceiptsContainer extends React.Component {
    constructor() {
        super();
        this.onPageChange = this.onPageChange.bind(this);
        this.handleSort = this.handleSort.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.deleteFilter = this.deleteFilter.bind(this);

        this.state = {
            activeSortParam: 'параметрам',
            buttonSort: [
                {name: 'По возрастанию рейтинга', data: 'rating'},
                {name: 'По убыванию рейтинга', data: '-rating'},
                {name: 'Вначале самые сложные', data: '-difficulty'},
                {name: 'Вначале самые легкие', data: 'difficulty'},
            ],
            filters: {
                diet: {
                    activeFilter: 'Выбрать:',
                    id: 'diet',
                    filterName: "Диета:",
                    filtersParams: [
                        {name: 'Диета 1200 ккал', data: '1200ccal',},
                        {name: 'Белковая диета', data: 'protein'},
                        {name: 'Спортивная диета', data: 'sport'},
                        {name: 'Безуглеводная диета', data: 'carbohydrateFree'},
                    ],
                },
                eating: {
                    activeFilter: 'Выбрать:',
                    id: 'eating',
                    filterName: "Время приема пищи:",
                    filtersParams: [
                        {name: 'Завтрак', data: 'breakfast'},
                        {name: 'Второй завтрак', data: 'secondBreakfast'},
                        {name: 'Обед', data: 'lunch'},
                        {name: 'Перекус', data: 'snack'},
                        {name: 'Ужин', data: 'dinner'},
                    ],
                }
            },
        };

    }


    deleteFilter = (e) => {
        const {diet, eating} = this.state.filters;
        let deleteFilter = e.target.parentNode.firstChild;
        if (deleteFilter.innerHTML === diet.filterName) {
            this.props.recipes.currentFilter.diet = '';
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    diet: {
                        ...this.state.filters.diet,
                        activeFilter: 'Выбрать:'
                    }
                }
            });
        }
        else if (deleteFilter.innerHTML === eating.filterName) {
            this.props.recipes.currentFilter.eating = '';
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    eating: {
                        ...this.state.filters.eating,
                        activeFilter: 'Выбрать:'
                    }

                }
            });
        }

        this.props.recipesFetchData(this.props.recipes.currentFilter);
    };
    onPageChange = (e, currentPageNew) => {
        this.props.recipes.currentFilter.startPage = currentPageNew;
        this.props.recipesFetchData(this.props.recipes.currentFilter);
    };

    handleSort = (e) => {
        this.props.recipes.currentFilter.sort = this.state.buttonSort
            .filter(item => item.data === e.target.dataset.sort)
            .map(item => item.data)
            .join();
        this.setState({
            activeSortParam: e.target.innerHTML
        });
        this.props.recipesFetchData(this.props.recipes.currentFilter);
    };
    handleFilter(e) {

        const {diet, eating} = this.state.filters;
        if (e.target.closest('div').id === 'diet') {

            let currentDiet = diet.filtersParams
                .filter(item => item.data === e.target.dataset.sort)
                .map(item => item.name)
                .join();

            this.props.recipes.currentFilter.diet = currentDiet;
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    diet: {
                        ...this.state.filters.diet,
                        activeFilter: currentDiet
                    }

                }
            });
        } else if (e.target.closest('div').id === 'eating') {
            let currentEating = eating.filtersParams
                .filter(item => item.data === e.target.dataset.sort)
                .map(item => item.name)
                .join();

            this.props.recipes.currentFilter.eating = currentEating;
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    eating: {
                        ...this.state.filters.eating,
                        activeFilter: currentEating
                    }

                }
            });
        }
        this.props.recipesFetchData(this.props.recipes.currentFilter);
    }
    componentDidMount() {
        this.props.recipesFetchData(this.props.recipes.currentFilter);
    }
    render() {
        const {items, loading, errors, currentFilter} = this.props.recipes;
        const {totalRecipesCount, startPage, pageSize, sort} = currentFilter;

        const renderRecipes = items.map((item) => {
            return (
                <ReceiptCard key={item._id} {...item}/>
            )
        });
        return (
            <div>
                <div className='mt-5'>
                    <FindRecipes/>
                    <ButtonSortFilter activeSortParam={this.state.activeSortParam} buttonSort={this.state.buttonSort}
                                      filters={this.state.filters}
                                      handleSort={this.handleSort.bind(this)}
                                      handleFilter={this.handleFilter.bind(this)}
                                      deleteFilter={this.deleteFilter.bind(this)}/>
                </div>
                <div className={s.container}>
                    {loading ? <Preloader/> : renderRecipes}
                    {(!loading && totalRecipesCount === 0) && <NoResultsFoundRecipes/>}
                </div>

                    <Paginator totalProgramsCount={totalRecipesCount} pageSize={pageSize} currentPage={startPage}
                               onPageChange={this.onPageChange.bind(this)}/>
            </div>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        recipes: state.recipes,
    };
};
export default connect(mapStateToProps, {recipesFetchData})(ReceiptsContainer);


