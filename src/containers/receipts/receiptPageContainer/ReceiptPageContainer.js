import React from 'react'
import axios from 'axios';
import ReceiptPageInfo from "../../../components/receiptsItems/receiptPage/receiptPageInfo/ReceiptPageInfo";
import ReceiptPageDirection from "../../../components/receiptsItems/receiptPage/receiptDirection/ReceiptDirection";
import Preloader from "../../../components/common/preloader/Preloader";

class ReceiptPageContainer extends React.Component {
    state = {
        resultsReceiptImg: [],
        fullReceipt: {},
        loading: true,
    };

    componentDidMount() {
        const receipts = this.props.match.params;
        const receiptNo = receipts.itemNo;
        console.log(receiptNo);
        axios.get(`/receipts/${receiptNo}`)
            .then(rec => {
                this.setState(
                    {
                        fullReceipt: rec.data,
                        loading: false
                    }
                );
                const resultsReceipt = rec.data;
                this.setState(
                    {
                        resultsReceiptImg: resultsReceipt.imgIngredient
                    }
                );
            })
    }

    render() {
        const {resultsReceiptImg, fullReceipt, loading} = this.state;
        // const receiptItem = <ReceiptPageCarousel img={resultsReceiptImg}/>;
        const receiptPageInfo = <ReceiptPageInfo {...fullReceipt}/>;
        const receiptDirection = <ReceiptPageDirection {...fullReceipt}/>;
        return (
            <div>
                {loading ? <Preloader/> : [
                    <div className='pt-4'>
                        <div className='d-flex justify-content-around row'>
                            {/*{receiptItem}*/}
                            {receiptPageInfo}
                        </div>
                        {receiptDirection}
                    </div>

                ]}
            </div>
        )
    }
}

export default ReceiptPageContainer;
