import React from "react";

import s from './calculator.module.scss';
import WasteOfCalories from "../../components/calculatorItems/wasteOfCalories/WasteOfCalories";
import NeededLiquid from "../../components/calculatorItems/neededLiquid/NeededLiquid";
import BodyMassIndex from "../../components/calculatorItems/bodyMassIndex/BodyMassIndex";

export default class Calculator extends React.Component {

    render() {
        return (
            <div  className={`${s.wrapper} d-flex flex-column flex-md-column flex-lg-row col-8 col-lg-12`}>
                <WasteOfCalories/>
                <NeededLiquid/>
                <BodyMassIndex/>
            </div>
        )
    }
}
