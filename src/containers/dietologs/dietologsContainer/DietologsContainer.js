import React, {Component} from 'react'
import s from './dietologsContainer.module.scss'
import DietologCard from '../../../components/dietologsItems/dietologCard/DietologCard';
import Preloader from "../../../components/common/preloader/Preloader";
import {connect} from "react-redux";
import {itemsFetchDataDietologs} from "../../../store/actions/fetcthDataDietologsAction";
import Paginator from "../../../components/common/Paginator/Paginator";
import ButtonSortFilter from "../../../components/common/button/buttonFilter/ButtonSortFilter";
import NoResultsFound from "../../../components/common/noResultsFound/NoResultsFound";

export class DietologsContainer extends React.Component {
    constructor(props) {
        super(props);
        this.onPageChange = this.onPageChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.deleteFilter = this.deleteFilter.bind(this);
        this.state = {
            activeSortParam: 'параметрам',
            buttonSort: [
                {name: 'по возрастанию цены за консультацию', data: 'currentPrice'},
                {name: 'по убыванию цены за консультацию', data: '-currentPrice'},
                {name: 'По возрастанию рейтинга', data: 'rating'},
                {name: 'По убыванию рейтинга', data: '-rating'},
                {name: 'По возрастанию опыта работы', data: 'experience'},
                {name: 'По убыванию опыта работы', data: '-experience'},
            ],
            filters: {
                sex: {
                    activeFilter: 'Выбрать:',
                    id: 'sex',
                    filterName: "В зависимости от пола:",
                    filtersParams: [
                        {name: 'женщина', data: 'female',},
                        {name: 'мужчина', data: 'male'},

                    ],
                },
                qualification: {
                    activeFilter: 'Выбрать:',
                    id: 'qualification',
                    filterName: "Квалификационная категория:",
                    filtersParams: [
                        {name: 'Врач первой категории', data: 'first'},
                        {name: 'Врач второй категории', data: 'second'},
                        {name: 'Врач высшей категории', data: 'high'}
                    ],
                }
            },

        };
    }

    onPageChange = (e, currentPageNew) => {
        this.props.items.currentFilter.startPage = currentPageNew;
        this.props.itemsFetchDataDietologs(this.props.items.currentFilter);
    };
    handleSort = (e) => {
        this.props.items.currentFilter.sort = this.state.buttonSort
            .filter(item => item.data === e.target.dataset.sort)
            .map(item => item.data)
            .join();
        this.setState({
            activeSortParam: e.target.innerHTML
        });

        this.props.itemsFetchDataDietologs(this.props.items.currentFilter);
    };

    handleFilter(e) {
        const {sex, qualification} = this.state.filters;
        if (e.target.closest('div').id === 'sex') {

            let currentSex = sex.filtersParams
                .filter(item => item.data === e.target.dataset.sort)
                .map(item => item.name)
                .join();

            this.props.items.currentFilter.sex = currentSex;
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    sex: {
                        ...this.state.filters.sex,
                        activeFilter: currentSex
                    }

                }
            });
        } else if (e.target.closest('div').id === 'qualification') {
            let currentQualification = qualification.filtersParams
                .filter(item => item.data === e.target.dataset.sort)
                .map(item => item.name)
                .join();

            this.props.items.currentFilter.qualification = currentQualification;
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    qualification: {
                        ...this.state.filters.qualification,
                        activeFilter: currentQualification
                    }

                }
            });
        }
        this.props.itemsFetchDataDietologs(this.props.items.currentFilter);
    }

    deleteFilter = (e) => {
        const {sex, qualification} = this.state.filters;
        let deleteFilter = e.target.parentNode.firstChild;
        if (deleteFilter.innerHTML === sex.filterName) {
            this.props.items.currentFilter.sex = '';
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    sex: {
                        ...this.state.filters.sex,
                        activeFilter: 'Выбрать:'
                    }
                }
            });
        }
        else if (deleteFilter.innerHTML === qualification.filterName) {
            this.props.items.currentFilter.qualification = '';
            this.setState({
                ...this.state,
                filters: {
                    ...this.state.filters,
                    qualification: {
                        ...this.state.filters.qualification,
                        activeFilter: 'Выбрать:'
                    }

                }
            });
        }

        this.props.itemsFetchDataDietologs(this.props.items.currentFilter);
    };
    componentDidMount() {
        this.props.itemsFetchDataDietologs(this.props.items.currentFilter);
    }

    render() {
        const {items, loading, errors, currentFilter} = this.props.items;
        const {totalDietologsCount, startPage, pageSize, sort, sex, qualification} = currentFilter;

        const renderDietologs = items.map((dietolog) => {
            return (
                <DietologCard key={dietolog._id} {...dietolog}/>
            )
        });

        return (
            <div>
                <div className='mt-5'>
                    <ButtonSortFilter activeSortParam={this.state.activeSortParam} buttonSort={this.state.buttonSort}
                                      filters={this.state.filters}
                                      handleSort={this.handleSort.bind(this)}
                                      handleFilter={this.handleFilter.bind(this)}
                                      deleteFilter={this.deleteFilter.bind(this)}/>
                </div>
                <div className={s.container}>
                    {loading ? <Preloader/> : renderDietologs}
                    {(!loading && totalDietologsCount === 0) && <NoResultsFound/>}
                </div>

                    <Paginator totalProgramsCount={totalDietologsCount}
                               pageSize={pageSize}
                               currentPage={startPage}
                               onPageChange={this.onPageChange.bind(this)}
                    />

            </div>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        items: state.dietologs
    };
};
export default connect(mapStateToProps, {itemsFetchDataDietologs})(DietologsContainer);


