import React from 'react'
import axios from 'axios';
import DietologPage from "../../../components/dietologsItems/dietologPage/DietologPage";
import Preloader from "../../../components/common/preloader/Preloader";

class DietologPageContainer extends React.Component {
    state = {
        resultsDietolog: {
            procedures: [],
            progress: [],
            study: [],
            treatment: [],
        },
        loading: true
    };

    componentDidMount() {
        const dietologs = this.props.match.params;
        const dietologNo = dietologs.itemNo;
        axios.get(`/dietologs/${dietologNo}`)
            .then(rec => {
                const resultsDietolog = rec.data;
                this.setState(
                    {
                        loading: false,
                        resultsDietolog: resultsDietolog
                    }
                );
            })
    }

    render() {
        const {resultsDietolog, loading} = this.state;
        return (
            <div>
                {loading ? <Preloader/> : <DietologPage {...resultsDietolog}/>}
            </div>
        )
    }
}

export default DietologPageContainer;
