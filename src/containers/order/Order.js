import React, {Component} from 'react';
import s from './order.module.scss';
import {connect} from "react-redux";
import axios from "axios";
import validator from "validator";
import OrderedItems from "../../components/orderItems/orderedItems/OrderedItems";
import OrderForm from "../../components/orderItems/orderForm/OrderForm";
import OrderSuccess from "../../components/orderItems/orderSuccess/OrderSuccess";
import {clearCart} from "../../store/actions/cartAction";
export class Order extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: ['Имя и фамилия:', 'Ел.почта:', 'Мобильный телефон:'],
            formParam: [{name: "customerName", val: ""}, {name: "email", val: ""}, {name: "mobile", val: ""}],
            orderNo: '',
            checked: false,
        };
      this.placeOrder = this.placeOrder.bind(this);
      this.changeHandler = this.changeHandler.bind(this);
      this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }
    handleCheckboxChange = (e) => {
        this.setState({
            checked: e.target.checked
        });
    };
    changeHandler = (e) => {
        let newItems = this.state.formParam.map(item => item.name === e.target.dataset.name
            ? {...item, val: e.target.value}
            : item);
        this.setState({
                formParam: newItems
            }
        );
        e.target.value.length >= 1 && e.target.classList.remove('input-mistake')
    };

    validate = (e) => {
        const [customerName, email, mobile] = this.state.formParam;
        let elems = document.querySelectorAll('input');

        if (!customerName.val) {
            elems[0].classList.add('input-mistake')
        }

        if (!(validator.isEmail(email.val))) {
            elems[1].classList.add('input-mistake')
        }

        if (!mobile.val || isNaN(mobile.val)) {
            elems[2].classList.add('input-mistake')
        } else {
            return true
        }
    };

    placeOrder = (e) => {
        const [customerName, email, mobile] = this.state.formParam;
        const {programs, dietologs, totalSum} = this.props;
        e.preventDefault();
        const isValid = this.validate(e);
        if (isValid && this.state.checked) {
            let newOrder = {
                customerName: customerName.val,
                email: email.val,
                totalSum: totalSum,
                mobile: mobile.val,
                programs: programs,
                dietologs: dietologs
            };
            axios.post('/orders', newOrder)
                .then(newOrder => {
                    const orderData = newOrder.data.order;
                    this.setState({
                        orderNo: orderData.orderNo,
                    });
                })
                .catch(error =>
                    console.log(error)
                );
            this.props.clearCart();
        }
    };

    render() {
        return (
            <div className={`${s.wrapper} col-8`}>
                <div>
                    {this.state.orderNo === ''
                        ? [
                            <div>
                                <p className={s.title}>Оформление заказа</p>
                                <div className={s.container}>
                                    <OrderedItems />
                                    <OrderForm fields={this.state.fields} formParam={this.state.formParam}
                                               changeHandler={this.changeHandler.bind(this)}
                                               placeOrder={this.placeOrder.bind(this)}
                                               handleCheckboxChange={this.handleCheckboxChange.bind(this)}
                                               checked={this.state.checked}
                                    />
                                </div>
                            </div>
                        ]
                        : <OrderSuccess orderNo={this.state.orderNo}/>
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        order: state.order
    };
};
export default connect(mapStateToProps,{clearCart})(Order);

