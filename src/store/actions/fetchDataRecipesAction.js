
import axios from "axios";


export function recipesFetchDataSuccess(recipes, currentFilter) {
    let {totalRecipesCount, startPage, pageSize, sort, eating, diet} = currentFilter;
    return {
        type: 'ITEMS_FETCH_DATA_SUCCESS_RECIPES',
        payload: recipes,
        quantity: totalRecipesCount,
        sort: sort,
        startPage: startPage,
        eating: eating,
        diet: diet
    };
}


export function recipesFetchData(currentFilter) {
    let {totalRecipesCount, startPage, pageSize, sort, eating, diet} = currentFilter;

    const queryString = require('query-string');
    const url = queryString.stringifyUrl({
        url: '/receipts/filter?',
        query:
            {
                perPage: pageSize,
                startPage: startPage,
                sort: sort,
                eating: eating,
                diet: diet
            },
    },{
        skipEmptyString: true
    });


    return (dispatch) => {
        dispatch(recipesAreLoading(true));

        axios.get(url)
            .then(res => {
                dispatch(recipesAreLoading(false));
                // console.log(res.data)
                const {recipes, recipesQuantity} = res.data;


                currentFilter.totalRecipesCount = recipesQuantity;

                // console.log(recipes)
                dispatch(recipesFetchDataSuccess(recipes, currentFilter))
            })
            .catch(error => {
                dispatch(recipesHaveErrored(true));
                console.log(error.response)
            });

    };
}
export function searchRecipes(recipes) {
    const recipesQuantity = recipes.length;
    return{
        type: 'SEARCHED_RECIPES',
        payload: recipes,
        quantity: recipesQuantity
    }

}
export function recipesHaveErrored(bool) {
    return {
        type: 'RECIPES_HAVE_ERRORED',
        payload: bool
    };
}

export function recipesAreLoading(bool) {
    return {
        type: 'RECIPES_ARE_LOADING',
        payload: bool
    };
}
