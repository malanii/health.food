import {PLACE_ORDER} from "../../constants/types";
export function placeOrder(e, total, itemsProgram, itemsDietologs) {
    return {
        type: PLACE_ORDER,
        total: total,
        dietologs: itemsDietologs,
        programs: itemsProgram

    };
}