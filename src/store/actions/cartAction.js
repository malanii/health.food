import {
    ADD_TO_CART_PROGRAM,
    REMOVE_PROGRAM,
    ADD_TO_CART_DIETOLOG, REMOVE_DIETOLOG, INC_CART_PROGRAM_QUANTITY, DEC_CART_PROGRAM_QUANTITY, CLEAR_CART
} from "../../constants/types";


export const addToCartPrograms = (e,currentProgram) => {

    return {
        type: ADD_TO_CART_PROGRAM,
        payload: currentProgram,

    }
};


export const removeProgram = (e,itemNo) => {

    return {
        type:REMOVE_PROGRAM,
        payload: itemNo
    }
};

export const increaseQuantity = (e,itemNo) => {
    return {
        type:INC_CART_PROGRAM_QUANTITY,
        payload:itemNo
    }
};


export const decreaseQuantity = (e, itemNo) => {
    return {
        type: DEC_CART_PROGRAM_QUANTITY ,
        payload:itemNo
    }
};

export function addToCartDietolog(e,currentDietolog) {
    return {
        type: ADD_TO_CART_DIETOLOG,
        payload: currentDietolog
    };
}
export const removeDietolog = (e,itemNo) => {

    return{
        type: REMOVE_DIETOLOG,
        payload:itemNo
    }
};

export const clearCart = () => {
    return{
        type: CLEAR_CART,
        payload: []
    }
};
