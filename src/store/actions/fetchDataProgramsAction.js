//programs

import axios from "axios";

export function itemsFetchDataSuccess(programs, currentFilter) {
    const {totalProgramsCount, currentPage, startPage, sort, target, goals} = currentFilter;
    return {
        type: 'ITEMS_FETCH_DATA_SUCCESS',
        payload: programs,
        quantity: totalProgramsCount,
        goals: goals,
        sort: sort,
        startPage: startPage,
        target: target
    };
}


export function itemsFetchDataPrograms(currentFilter) {
    let {totalProgramsCount, startPage, pageSize, sort, target, goals} = currentFilter;

    const queryString = require('query-string');
    const url = queryString.stringifyUrl({
        url: '/programs/filter?',
        query:
            {
                perPage: pageSize,
                startPage: startPage,
                sort: sort,
                goals: goals,
                target: target
            },
    },{
        skipEmptyString: true
    });


    // console.log(url);

    return (dispatch) => {
        dispatch(programsAreLoading(true));

        axios.get(url)
            .then(res => {
                dispatch(programsAreLoading(false));
                const {programs, programsQuantity} = res.data;


                currentFilter.totalProgramsCount = programsQuantity;


                dispatch(itemsFetchDataSuccess(programs, currentFilter))
            })
            .catch(error => {
                dispatch(programsHaveErrored(true));
                console.log(error.response)
            });

    };
}

export function programsHaveErrored(bool) {
    return {
        type: 'PROGRAMS_HAVE_ERRORED',
        payload: bool
    };
}

export function programsAreLoading(bool) {
    return {
        type: 'PROGRAMS_ARE_LOADING',
        payload: bool
    };
}
