import axios from "axios";

export function itemsFetchDataSuccessDietologs(dietologs, currentFilter) {
    const {totalDietologsCount, startPage, sort,sex,qualification} = currentFilter;

    return {
        type: 'ITEMS_FETCH_DATA_SUCCESS_DIETOLOGS',
        payload: dietologs,
        quantity: totalDietologsCount,
        sort: sort,
        startPage: startPage,
        sex:sex,
        qualification: qualification
    };
}

export function itemsFetchDataDietologs(currentFilter) {
    let {totalDietologsCount, startPage, pageSize, sort,sex,qualification} = currentFilter;

    const queryString = require('query-string');
    const url = queryString.stringifyUrl({
        url: '/dietologs/filter?',
        query:
            {
                perPage: pageSize,
                startPage: startPage,
                sort: sort,
                sex: sex,
                qualification: qualification

            },
    },{
        skipEmptyString: true
    });


    return (dispatch) => {
       dispatch(dietologsAreLoading(true));


        axios.get(url)
            .then(res => {
                dispatch(dietologsAreLoading(false));

              // console.log('res dietologs',res.data);


                const {dietologs, dietologsQuantity} = res.data;
                // console.log('res dietologs',dietologs);
                // console.log('res dietologs',dietologsQuantity);


                currentFilter.totalDietologsCount = dietologsQuantity;
                dispatch(itemsFetchDataSuccessDietologs(dietologs, currentFilter))

            })
            .catch(error => {
                dispatch(dietologsHaveErrored(true));
                console.log(error.response)
            });

    };
}


export function dietologsHaveErrored(bool) {
    return {
        type: 'DIETOLOGS_HAVE_ERRORED',
        payload: bool
    };
}

export function dietologsAreLoading(bool) {
    return {
        type: 'DIETOLOGS_ARE_LOADING',
        payload: bool
    };
}

