import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import {composeWithDevTools} from "redux-devtools-extension";
import {persistedState} from '../commonHelpers/localStorage'
const configureStore = () => {
    return createStore(
        rootReducer,
        persistedState,
        composeWithDevTools(applyMiddleware(thunk)),
    );
};
export default configureStore;

