import * as types from '../../constants/types';

const initialState = {
    items: [],
    loading: false,
    errors: false,
    currentFilter: {
        totalProgramsCount: 0,
        startPage: 1,
        pageSize: 5,
        sort: '',
        goals: '',
        target: '',
    },
};

export function programs(state = initialState, action = {}) {
    switch (action.type) {
        case types.ITEMS_FETCH_DATA_SUCCESS:
            return {
                ...state,
                items: action.payload,
                currentFilter:
                    {
                        ...state.currentFilter,
                        totalProgramsCount: action.quantity,
                        startPage: action.startPage,
                        // pageSize: 5,
                        sort:action.sort,
                        goals: action.goals,
                        target: action.target
                    }
            };

        case types.PROGRAMS_ARE_LOADING:
            return {...state, loading:action.payload};
        case types.PROGRAMS_HAVE_ERRORED:
            return {...state, errors:action.payload};
        default:
            return state
    }
}

