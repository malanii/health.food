import * as types from '../../constants/types';

const initialState = {
    items: [],
    loading: false,
    errors: false,
    currentFilter: {
        totalRecipesCount: 0,
        startPage: 1,
        pageSize: 6,
        eating:'',
        diet:''

    },
};

export function recipes(state = initialState, action = {}) {
    switch (action.type) {
        case types.ITEMS_FETCH_DATA_SUCCESS_RECIPES:
            return {
                ...state,
                items: action.payload,
                currentFilter:
                    {
                        ...state.currentFilter,
                        totalRecipesCount: action.quantity,
                        startPage: action.startPage,
                        pageSize: 6,
                        sort:action.sort,
                        eating: action.eating,
                        diet: action.diet
                    }
            };
        case types.SEARCHED_RECIPES:
            return {...state, items:action.payload,
                currentFilter:
                    {
                        ...state.currentFilter,
                        totalRecipesCount: action.quantity,
                    }
            };
        case types.RECIPES_ARE_LOADING:
            return {...state, loading:action.payload};
        case types.RECIPES_HAVE_ERRORED:
            return {...state, errors:action.payload};

        default:
            return state
    }
}

