import * as types from '../../constants/types';


const initialState = {
    itemsProgram: [],
    itemsDietologs: '',
    total: 0
};


export function order(state = initialState, action = {}) {
    switch (action.type) {
        case types.PLACE_ORDER:
            return {
                ...state,
                total: action.total,
                itemsDietologs: action.dietologs,
                itemsProgram: action.programs
            };
        default:
            return state
    }
}