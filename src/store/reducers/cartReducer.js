import * as types from "../../constants/types";
const initialState = {
    itemsProgram: [],
    itemsDietologs: []
};
export function cart(state = initialState, action = {}) {
    switch (action.type) {
        case types.ADD_TO_CART_PROGRAM:

            return {
                ...state,
                itemsProgram: [...state.itemsProgram, action.payload],
            };

        case types.REMOVE_PROGRAM: {
            const itemsProgram = state.itemsProgram.filter(
                product => product.itemNo !== action.payload
            );
            return {...state, itemsProgram};
        }


        case types.DEC_CART_PROGRAM_QUANTITY : {

            const itemsProgram = state.itemsProgram.map(program => {
                return program.itemNo === action.payload && program.quantity > 1
                    ? {...program, quantity: program.quantity - 14, currentPrice: program.currentPrice - 250}
                    : {...program};
            });
            return {...state, itemsProgram};
        }


        case types.INC_CART_PROGRAM_QUANTITY: {
            const itemsProgram = state.itemsProgram.map(program => {
                return program.itemNo === action.payload
                    ? {...program, quantity: program.quantity + 14, currentPrice: program.currentPrice +  250}
                    : {...program};
            });

            return {...state, itemsProgram};
        }

        case types.ADD_TO_CART_DIETOLOG:

            return {
                ...state,
                itemsDietologs: [...state.itemsDietologs, action.payload],
            };

        case types.REMOVE_DIETOLOG: {
            const itemsDietologs = state.itemsDietologs.filter(
                dietolog => dietolog.itemNo !== action.payload
            );
            return {...state, itemsDietologs};
        }
        case types.CLEAR_CART: {
            const clearCart = [];
            const itemsProgram = clearCart;
            const itemsDietologs = clearCart;
            return {...state, itemsProgram, itemsDietologs};
        }

        default:
            return state

    }
}


