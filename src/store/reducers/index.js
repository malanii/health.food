import { combineReducers } from 'redux';
import {programs} from './programsReducer';
import {dietologs} from "./dietologsReducer";
import {order} from "./orderReducer";
import {cart} from "./cartReducer";
import {recipes} from "./recipesReducer";


export default combineReducers({
    programs,
    dietologs,
    cart,
    order,
    recipes

});

