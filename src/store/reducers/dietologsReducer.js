import * as types from '../../constants/types';

const initialState = {
    items: [],
    loading: false,
    errors: false,
    currentFilter: {
        totalDietologsCount: 0,
        startPage: 1,
        pageSize: 6,
        sort: '',
        sex:'',
        qualification:''
    },
};


export function dietologs(state = initialState, action = {}) {

    switch (action.type) {
        case types.ITEMS_FETCH_DATA_SUCCESS_DIETOLOGS:
            return {
                ...state,
                items: action.payload,
                currentFilter:
                    {
                        ...state.currentFilter,
                        totalDietologsCount: action.quantity,
                        startPage: action.startPage,
                        pageSize: 6,
                        sort:action.sort,
                        sex: action.sex,
                        qualification: action.qualification

                    }
            };
        case types.DIETOLOGS_ARE_LOADING:
            return {...state, loading:action.payload};
        case types.DIETOLOGS_HAVE_ERRORED:
            return {...state, errors:action.payload};
        // case types.SET_CURRENT_DIETOLOG_PAGE:
        //     return {...state, currentPage:action.payload};




        default:
            return state

    }
}