import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from "react-router-dom";

import {Provider} from 'react-redux';
import configureStore  from "./store/store";
import saveState from "./commonHelpers/localStorage";
const store = configureStore();
store.subscribe(() => {
   saveState({cart: store.getState().cart})
});




ReactDOM.render(
    <Provider store={store}>

        <BrowserRouter>
            <App/>
        </BrowserRouter>

    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
